/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.webflowdemo.webflow;

import java.io.Serializable;

/**
 *
 * @author rodrigokuntzer
 */
public class Counter implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	int confirmations;

	int cancellations;
	
	public void incrementConfirmations() {
		confirmations++;
	}
	
	public void incrementCancellations() {
		cancellations++;
	}

	public int getConfirmations() {
		return confirmations;
	}

	public int getCancellations() {
		return cancellations;
	}
}
