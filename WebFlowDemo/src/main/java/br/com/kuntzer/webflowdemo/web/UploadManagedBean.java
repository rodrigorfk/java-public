/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.webflowdemo.web;

import javax.faces.bean.ManagedBean;
import org.richfaces.event.FileUploadEvent;

/**
 *
 * @author rodrigokuntzer
 */
@ManagedBean
public class UploadManagedBean {
	
	public void uploadListener(FileUploadEvent event){
		System.out.println("UploadManagedBean.uploadListener");
	}
}
