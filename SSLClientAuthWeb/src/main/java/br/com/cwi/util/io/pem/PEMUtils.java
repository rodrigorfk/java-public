/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.util.io.pem;

import java.io.IOException;

/**
 *
 * @author kuntzer
 */
public class PEMUtils {

    private static final String PEM_CERT_BEGIN = "-----BEGIN CERTIFICATE-----";
    private static final String PEM_CERT_END   = "-----END CERTIFICATE-----";
    
    public static String generateFormatedCertificate(String singleLineString)throws IOException{
        if(singleLineString != null){
            if(!singleLineString.startsWith(PEM_CERT_BEGIN)){
                throw new IOException("problem parse cert: can't find BEGIN CERTIFICATE at string start.");
            }
            if(!singleLineString.endsWith(PEM_CERT_END)){
                throw new IOException("problem parse cert: can't find END CERTIFICATE at string end.");
            }
            String body = singleLineString.substring(PEM_CERT_BEGIN.length());
            body = body.substring(0, body.length()-PEM_CERT_END.length());
            body = body.replaceAll("\n", "");
            body = body.replaceAll(" ", "");
            
            StringBuilder buffer = new StringBuilder();
            buffer.append(PEM_CERT_BEGIN).append("\n");
            while(body.length() >= 64){
                buffer.append(body.substring(0, 64)).append("\n");
                body = body.substring(64);
            }
            if(body.length() != 0){
                buffer.append(body).append("\n");
            }
            buffer.append(PEM_CERT_END);
            return buffer.toString();
        }
        return null;
    }
}
