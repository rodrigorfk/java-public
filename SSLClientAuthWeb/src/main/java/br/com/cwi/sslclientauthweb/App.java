/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.sslclientauthweb;

import br.com.cwi.util.io.pem.PEMUtils;

/**
 *
 * @author kuntzer
 */
public class App {

    public static void main(String[] args) throws Exception {
        String pem = "-----BEGIN CERTIFICATE----- MIID3jCCA4igAwIBAgIBAjANBgkqhkiG9w0BAQUFADCBuDELMAkGA1UEBhMCQlIx GjAYBgNVBAgTEVJpbyBHcmFuZGUgZG8gU3VsMRUwEwYDVQQHFAxT428gTGVvcG9s ZG8xGjAYBgNVBAoTEUNXSSBTb2Z0d2FyZSBMVERBMR0wGwYDVQQLExROdWNsZW8g ZGUgVGVjbm9sb2dpYTEYMBYGA1UEAxMPUm9kcmlnbyBLdW50emVyMSEwHwYJKoZI hvcNAQkBFhJrdW50emVyQGN3aS5jb20uYnIwHhcNMTIwOTEzMTg1MjUzWhcNMTMw OTEzMTg1MjUzWjCBtzELMAkGA1UEBhMCQlIxGjAYBgNVBAgTEVJpbyBHcmFuZGUg ZG8gU3VsMRowGAYDVQQKExFDV0kgU29mdHdhcmUgTFREQTEdMBsGA1UECxMUTnVj bGVvIGRlIFRlY25vbG9naWExLjAsBgNVBAMUJUt1bnR6ZXIgVGVjbm9sb2dpYSBk YSBJbmZvcm1h5+NvIExUREExITAfBgkqhkiG9w0BCQEWEmt1bnR6ZXJAY3dpLmNv bS5icjCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA0QtIiyoCojLqDuADrjAx +rYmj5689HCfYp9mScc3R0SqubtPiYfWQkuUfAZneTqZdoJ+mPgwaBoOVxOifgl8 0GURGLOBJNAdDnjVfvN8858QwI0KQ+ep4jayffWdfypxXM9sZU5QmwCPimcowgWX yySTWSN1WgwB0PvYnZSf35MCAwEAAaOCATYwggEyMAkGA1UdEwQCMAAwLAYJYIZI AYb4QgENBB8WHU9wZW5TU0wgR2VuZXJhdGVkIENlcnRpZmljYXRlMB0GA1UdDgQW BBQB+9MJB3BdpCbbbvQ093Hknqd0WzCB1wYDVR0jBIHPMIHMoYG+pIG7MIG4MQsw CQYDVQQGEwJCUjEaMBgGA1UECBMRUmlvIEdyYW5kZSBkbyBTdWwxFTATBgNVBAcU DFPjbyBMZW9wb2xkbzEaMBgGA1UEChMRQ1dJIFNvZnR3YXJlIExUREExHTAbBgNV BAsTFE51Y2xlbyBkZSBUZWNub2xvZ2lhMRgwFgYDVQQDEw9Sb2RyaWdvIEt1bnR6 ZXIxITAfBgkqhkiG9w0BCQEWEmt1bnR6ZXJAY3dpLmNvbS5icoIJAM11q/SNhokG MA0GCSqGSIb3DQEBBQUAA0EAs+pg8ehIZ4eOBHY48G0QGFFQT0wJPHKPjMCJ4xG4 Zn6SxGWmritdWsEjCaEWBOZ5xjLzLH/Pov/L6VxHbFXgLw== -----END CERTIFICATE-----";
    

        String pem2 = PEMUtils.generateFormatedCertificate(pem);
        System.out.println(pem2);
        java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        org.bouncycastle.openssl.PEMReader pr = new org.bouncycastle.openssl.PEMReader(new java.io.StringReader(pem2));
        java.security.cert.X509Certificate cert = (java.security.cert.X509Certificate) pr.readObject();
        System.out.println(cert.getSubjectDN().getName());
        
        
    }
}
