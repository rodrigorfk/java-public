/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.sslclientauthweb;

import br.com.cwi.util.io.pem.PEMUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kuntzer
 */
@WebServlet(name = "AuthServlet", urlPatterns = {"/AuthServlet"})
public class AuthServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AuthServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AuthServlet at " + request.getContextPath() + "</h1>");
            this.handleAuth(request, out);
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }
    
    private void handleAuth(HttpServletRequest request, PrintWriter out){
        Enumeration<String> names = request.getAttributeNames();
        out.println("<h1>getAttributeNames</h1>");
        while(names.hasMoreElements()){
            String attribute = names.nextElement();
            Object value = request.getAttribute(attribute);
            out.println("<p><strong>"+attribute+":</strong> "+value+"</p>");
        }
        System.out.println("saiu getAttributeNames");
        
        out.println("<h1>getHeaderNames</h1>");
        names = request.getHeaderNames();
        while(names.hasMoreElements()){
            String attribute = names.nextElement();
            String value = request.getHeader(attribute);
            out.println("<p><strong>"+attribute+":</strong> "+value+"</p>");
        }
        System.out.println("saiu getHeaderNames");
        
        try{
            String pem = request.getHeader("ssl_client_cert");
            System.out.println(pem);
            if(pem != null){
                pem = PEMUtils.generateFormatedCertificate(pem);
                java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
                org.bouncycastle.openssl.PEMReader pr = new org.bouncycastle.openssl.PEMReader(new java.io.StringReader(pem)); 
                java.security.cert.X509Certificate cert = (java.security.cert.X509Certificate) pr.readObject();
                
                out.println("<p><strong>getSubjectDN:</strong> "+cert.getSubjectDN().getName()+"</p>");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
