/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository;

import br.com.rodrigo.kuntzer.ee6demo.entity.MenuAplicacao;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IMenuAplicacaoRepository extends IBaseRepository<MenuAplicacao, Long> {
	
	List<MenuAplicacao> obterMenusPrincipais();
}
