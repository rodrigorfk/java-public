/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.web.controller;

import br.com.rodrigo.kuntzer.ee6demo.entity.Cliente;
import br.com.rodrigo.kuntzer.ee6demo.entity.ClienteTelefone;
import br.com.rodrigo.kuntzer.ee6demo.entity.Estado;
import br.com.rodrigo.kuntzer.ee6demo.entity.TipoCliente;
import br.com.rodrigo.kuntzer.ee6demo.service.IClienteService;
import br.com.rodrigo.kuntzer.ee6demo.service.IEmpresaService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rodrigokuntzer
 */
@ManagedBean
public class ClienteController implements Serializable {

	private static final Logger _logger = LoggerFactory.getLogger(ClienteController.class);
	
	@Inject
	private IClienteService clienteService;
	
	@Inject
	private IEmpresaService empresaService;
	
	private List<Cliente> lista = new ArrayList<Cliente>();
	private Long id;
	
	private Cliente entity;
	
	private ClienteTelefone telefoneAtual;
	
	private List<SelectItem> itensComboTipoPessoa = new ArrayList<SelectItem>();
	private List<Estado> itensComboEstado = new ArrayList<Estado>();
	
	public void listar(){
		FacesContext context = FacesContext.getCurrentInstance();
		if(!context.isPostback()){
			this.lista = clienteService.obterTodosClientes();
			this.entity = new Cliente();
			this.entity.setEstado(new Estado());
			this.itensComboTipoPessoa.clear();
			for(TipoCliente tipo:TipoCliente.values()){
				this.itensComboTipoPessoa.add(new SelectItem(tipo, tipo.getDescription()));
			}
		}
	}
	
	public String editar(){
		return "/pages/clientes/clientesForm?faces-redirect=true&id="+id;
	}
	
	public String novo(){
		return "/pages/clientes/clientesForm?faces-redirect=true";
	}

	public void onEdit(){
		FacesContext context = FacesContext.getCurrentInstance();
		if(!context.isPostback()){
			this.itensComboTipoPessoa.clear();
			for(TipoCliente tipo:TipoCliente.values()){
				this.itensComboTipoPessoa.add(new SelectItem(tipo, tipo.getDescription()));
			}

			this.itensComboEstado = empresaService.obterTodosEstados();

			if(this.id == null){
				this.entity = new Cliente();
			}else{
				this.entity = clienteService.obterClientePorId(this.id);
			}
		}
	}
	
	public String goToList(){
		return "/pages/clientes/clientesList?faces-redirect=true";
	}
	
	public void salvar(){
		FacesContext context = FacesContext.getCurrentInstance();
		try{
			clienteService.salvarCliente(this.entity);

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente salvo com sucesso!", null));
		}catch(Exception ex){
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao salvar o cliente: "+ex.getLocalizedMessage(), null));
			_logger.error("Erro no salvamento do cliente.", ex);
		}
	}
	
	public void excluirTelefone(ClienteTelefone telefone){
		this.entity.getTelefones().remove(telefone);
	}
	
	public void novoTelefone(){
		this.telefoneAtual = new ClienteTelefone();
	}
	
	public void salvarTelefone(){
		this.telefoneAtual.setCliente(this.entity);
		this.entity.getTelefones().add(this.telefoneAtual);
	}
	
	public void pesquisar(){
		this.lista = clienteService.obterClientesPesquisa(this.entity);
	}
	
	public List<Cliente> getLista() {
		return lista;
	}

	public void setLista(List<Cliente> lista) {
		this.lista = lista;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getEntity() {
		return entity;
	}

	public void setEntity(Cliente entity) {
		this.entity = entity;
	}

	public List<SelectItem> getItensComboTipoPessoa() {
		return itensComboTipoPessoa;
	}

	public void setItensComboTipoPessoa(List<SelectItem> itensComboTipoPessoa) {
		this.itensComboTipoPessoa = itensComboTipoPessoa;
	}

	public List<Estado> getItensComboEstado() {
		return itensComboEstado;
	}

	public void setItensComboEstado(List<Estado> itensComboEstado) {
		this.itensComboEstado = itensComboEstado;
	}

	public ClienteTelefone getTelefoneAtual() {
		return telefoneAtual;
	}

	public void setTelefoneAtual(ClienteTelefone telefoneAtual) {
		this.telefoneAtual = telefoneAtual;
	}

	public void setClienteService(IClienteService clienteService) {
		this.clienteService = clienteService;
	}

	public void setEmpresaService(IEmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
}
