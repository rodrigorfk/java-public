/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author rodrigokuntzer
 */
@Entity
@Table(name="CLIENTE_TELEFONE")
public class ClienteTelefone extends BaseEntity<Long> {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="ID_CLIENTE")
	private Cliente cliente;
	
	@Column(length=20)
	private String telefone;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	
}
