/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository;

import br.com.rodrigo.kuntzer.ee6demo.entity.Cliente;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IClienteRepository extends IBaseRepository<Cliente, Long> {
	
	List<Cliente> obterClientesPesquisa(Cliente cliente);
	
}
