/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.MenuAplicacao;
import br.com.rodrigo.kuntzer.ee6demo.repository.IMenuAplicacaoRepository;
import java.util.List;
import javax.inject.Named;

/**
 *
 * @author rodrigokuntzer
 */
@Named
public class MenuAplicacaoRepository extends JPABaseRepository<MenuAplicacao, Long> implements IMenuAplicacaoRepository {

	@Override
	public List<MenuAplicacao> obterMenusPrincipais() {
		return entityManager.createQuery("from MenuAplicacao where pai is null order by ordem", MenuAplicacao.class).getResultList();
	}
	
}
