/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.Empresa;
import br.com.rodrigo.kuntzer.ee6demo.repository.IEmpresaRepository;
import javax.inject.Named;

/**
 *
 * @author rodrigokuntzer
 */
@Named
public class EmpresaRepository extends JPABaseRepository<Empresa, Long> implements IEmpresaRepository {

	
}
