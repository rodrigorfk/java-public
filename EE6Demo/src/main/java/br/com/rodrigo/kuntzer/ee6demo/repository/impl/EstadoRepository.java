/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.Estado;
import br.com.rodrigo.kuntzer.ee6demo.repository.IEstadoRepository;
import java.util.List;
import javax.inject.Named;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author rodrigokuntzer
 */
@Named
public class EstadoRepository extends JPABaseRepository<Estado, Long> implements IEstadoRepository{

	@Override
	public List<Estado> obterTodos() {
		Session session = (Session) entityManager.getDelegate();
		Query consulta = session.createQuery("from Estado e order by e.sigla");
		consulta.setCacheable(true).setCacheRegion("regiaoEstados");
		
		return consulta.list();
	}
	
}
