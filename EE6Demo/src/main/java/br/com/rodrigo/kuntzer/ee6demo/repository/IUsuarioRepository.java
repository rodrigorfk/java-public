/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository;

import br.com.rodrigo.kuntzer.ee6demo.entity.Empresa;
import br.com.rodrigo.kuntzer.ee6demo.entity.Usuario;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IUsuarioRepository extends IBaseRepository<Usuario, Long> {
	
	List<Empresa> obterEmpresasUsuario(String username);
	
	Usuario obterUsuarioPorUsername(String username);
	
	Usuario obterUsuarioPorUsernameAndEmpresa(String username, Long empresa);
}
