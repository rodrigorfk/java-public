/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.BaseEntity;
import br.com.rodrigo.kuntzer.ee6demo.repository.IBaseRepository;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * 
 * @author rodrigokuntzer
 */
public abstract class JPABaseRepository<T extends BaseEntity<ID>, ID extends Serializable> implements IBaseRepository<T, ID> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public JPABaseRepository() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public List<T> obterTodos() {
		return entityManager.createQuery("from "+persistentClass.getSimpleName(), persistentClass).getResultList();
	}

	@Override
	public List<T> obterTodosPaginado(Integer inicial, Integer resultados) {
		return entityManager.createQuery("from "+persistentClass.getSimpleName(), persistentClass)
				.setFirstResult(inicial)
				.setMaxResults(inicial+resultados)
				.getResultList();
	}

	@Override
	public T salvar(T entity) {
		if(entity.getId() == null){
			entityManager.persist(entity);
		}
		return entityManager.merge(entity);
	}

	@Override
	public T obterPorId(ID id) {
		return entityManager.find(persistentClass, id);
	}

	@Override
	public Long obterNumeroResultados() {
		return entityManager.createQuery("select count(c) from "+persistentClass.getSimpleName()+" c", Long.class)
				.getSingleResult();
	}

}
