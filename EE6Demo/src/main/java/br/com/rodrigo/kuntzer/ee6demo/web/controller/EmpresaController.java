/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.web.controller;

import br.com.rodrigo.kuntzer.ee6demo.entity.Empresa;
import br.com.rodrigo.kuntzer.ee6demo.service.IEmpresaService;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author rodrigokuntzer
 */
@ManagedBean
@ViewScoped
public class EmpresaController implements Serializable{
	
	@Inject
	private IEmpresaService empresaService;
	
	private Empresa entity;
	private Long id;
	
	public List<Empresa> getTodasEmpresas(){
		return empresaService.obterTodasEmpresas();
	}
	
	public void initForm(){
		if(this.id == null){
			this.entity = new Empresa();
		}else{
			this.entity = empresaService.obterEmpresaPorId(id);
		}
	}
	
	public String novo(){
		return "/pages/empresa/empresaForm?faces-redirect=true";
	}
	
	public String editar(){
		return "/pages/empresa/empresaForm?faces-redirect=true&id="+this.id;
	}
	
	public String salvar(){
		this.empresaService.salvarEmpresa(this.entity);
		return "/pages/empresa/empresaList?faces-redirect=true";
	}

	public Empresa getEntity() {
		return entity;
	}

	public void setEntity(Empresa entity) {
		this.entity = entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEmpresaService(IEmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
}
