/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author rodrigokuntzer
 */
@Entity
@Table(name="MENU_APLICACAO", uniqueConstraints={
	@UniqueConstraint(columnNames={"MENU_PAI","LABEL"}, name="UK_MENU_APLICACAO")
})
public class MenuAplicacao extends BaseEntity<Long> {
	
	@Id
	private Long id;
	
	@Column(length=50,nullable=false)
	private String label;
	
	@Column(length=50,nullable=true)
	private String icon;
	
	@Column(length=50,nullable=true)
	private String url;

	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="MENU_PAI", nullable=true)
	private MenuAplicacao pai;
	
	@Column(nullable=false)
	private Boolean separador;
	
	@Column(nullable=false)
	private Integer ordem;
	
	@OrderBy("ordem")
	@OneToMany(cascade= CascadeType.ALL, mappedBy="pai")
	private List<MenuAplicacao> filhos = new ArrayList<MenuAplicacao>();
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public MenuAplicacao getPai() {
		return pai;
	}

	public void setPai(MenuAplicacao pai) {
		this.pai = pai;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Boolean getSeparador() {
		return separador;
	}

	public void setSeparador(Boolean separador) {
		this.separador = separador;
	}

	public List<MenuAplicacao> getFilhos() {
		return filhos;
	}

	public void setFilhos(List<MenuAplicacao> filhos) {
		this.filhos = filhos;
	}
	
}
