/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.service.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.Empresa;
import br.com.rodrigo.kuntzer.ee6demo.entity.Estado;
import br.com.rodrigo.kuntzer.ee6demo.repository.IEmpresaRepository;
import br.com.rodrigo.kuntzer.ee6demo.repository.IEstadoRepository;
import br.com.rodrigo.kuntzer.ee6demo.service.IEmpresaService;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author rodrigokuntzer
 */
@Named
@Stateless
public class EmpresaService implements IEmpresaService {

	@Inject
	private IEmpresaRepository empresaRepository;

	@Inject
	private IEstadoRepository estadoRepository;
	
	@Override
	public List<Empresa> obterTodasEmpresas() {
		return empresaRepository.obterTodos();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void salvarEmpresa(Empresa empresa) {
		empresaRepository.salvar(empresa);
	}

	@Override
	public Empresa obterEmpresaPorId(Long id) {
		return empresaRepository.obterPorId(id);
	}

	@Override
	public List<Estado> obterTodosEstados() {
		return estadoRepository.obterTodos();
	}

	@Override
	public Estado obterEstadoPorId(Long id) {
		return estadoRepository.obterPorId(id);
	}

	@Override
	public void salvarEstado(Estado estado) {
		estadoRepository.salvar(estado);
	}

	@Override
	public List<Estado> obterTodosEstadosPaginado(Integer inicial, Integer resultados) {
		return estadoRepository.obterTodosPaginado(inicial, resultados);
	}

	@Override
	public Long obterNumeroEstados() {
		return estadoRepository.obterNumeroResultados();
	}

	public void setEmpresaRepository(IEmpresaRepository empresaRepository) {
		this.empresaRepository = empresaRepository;
	}

	public void setEstadoRepository(IEstadoRepository estadoRepository) {
		this.estadoRepository = estadoRepository;
	}
	
	
}
