/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.Empresa;
import br.com.rodrigo.kuntzer.ee6demo.entity.Usuario;
import br.com.rodrigo.kuntzer.ee6demo.repository.IUsuarioRepository;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import org.hibernate.CacheMode;
import org.hibernate.Session;

/**
 *
 * @author rodrigokuntzer
 */
@Named
public class UsuarioRepository extends JPABaseRepository<Usuario, Long> implements IUsuarioRepository {

	@Override
	public List<Empresa> obterEmpresasUsuario(String username) {
		Session session = (Session) entityManager.getDelegate();
		org.hibernate.Query consulta =  session.createQuery("select u from Usuario u where u.username =:param1")
				.setParameter("param1", username)
				.setCacheable(true).setCacheRegion("regiao1")
				.setCacheMode(CacheMode.NORMAL);
		Usuario usuario = (Usuario) consulta.uniqueResult();	
		if(usuario != null){
			return usuario.getEmpresas();
		}
		return new ArrayList<Empresa>();
	}

	@Override
	public Usuario obterUsuarioPorUsername(String username) {
		List<Usuario> usuarios = entityManager.createQuery("select u from Usuario u where u.username =:username")
				.setParameter("username", username)
				.getResultList();
		if(!usuarios.isEmpty()){
			return usuarios.get(0);
		}
		return null;
	}

	@Override
	public Usuario obterUsuarioPorUsernameAndEmpresa(String username, Long empresa) {
		List<Usuario> usuarios = entityManager.createQuery("select u from Usuario u join u.empresas e where u.username =:username and e.id =:empresa")
				.setParameter("username", username)
				.setParameter("empresa", empresa)
				.getResultList();
		if(!usuarios.isEmpty()){
			return usuarios.get(0);
		}
		return null;
	}

	
}
