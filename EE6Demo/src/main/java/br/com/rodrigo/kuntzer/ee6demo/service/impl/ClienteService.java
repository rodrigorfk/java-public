/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.service.impl;

import br.com.rodrigo.kuntzer.ee6demo.entity.Cliente;
import br.com.rodrigo.kuntzer.ee6demo.repository.IClienteRepository;
import br.com.rodrigo.kuntzer.ee6demo.service.IClienteService;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author rodrigokuntzer
 */
@Named
public class ClienteService implements IClienteService {

	@Inject
	private IClienteRepository clienteRepository;
	
	@Override
	public List<Cliente> obterTodosClientes() {
		return clienteRepository.obterTodos();
	}

	@Override
	public Cliente obterClientePorId(Long id) {
		return clienteRepository.obterPorId(id);
	}

	@Override
	public Cliente salvarCliente(Cliente cliente) {
		return clienteRepository.salvar(cliente);
	}

	@Override
	public List<Cliente> obterClientesPesquisa(Cliente cliente) {
		return clienteRepository.obterClientesPesquisa(cliente);
	}

	public void setClienteRepository(IClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}
	
}
