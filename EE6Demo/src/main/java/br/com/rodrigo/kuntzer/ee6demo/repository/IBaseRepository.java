/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository;

import br.com.rodrigo.kuntzer.ee6demo.entity.BaseEntity;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IBaseRepository<T extends BaseEntity<ID>, ID extends Serializable> {
	
	List<T> obterTodos();
	List<T> obterTodosPaginado(Integer inicial, Integer resultados);
	T salvar(T entity);
	T obterPorId(ID id);
	Long obterNumeroResultados();
}
