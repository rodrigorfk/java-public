/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.kuntzer.ee6demo.repository;

import br.com.rodrigo.kuntzer.ee6demo.entity.Estado;

/**
 *
 * @author rodrigokuntzer
 */
public interface IEstadoRepository extends IBaseRepository<Estado, Long> {
	
}
