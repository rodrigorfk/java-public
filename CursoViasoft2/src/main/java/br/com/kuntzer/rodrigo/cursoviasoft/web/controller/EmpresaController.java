/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.controller;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IEmpresaService;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author rodrigokuntzer
 */
@Controller
@Scope("view")
public class EmpresaController implements Serializable{
	
	@Autowired
	private IEmpresaService empresaService;
	
	private Empresa entity;
	private Long id;
	
	public List<Empresa> getTodasEmpresas(){
		return empresaService.obterTodasEmpresas();
	}
	
	public void initForm(){
		if(this.id == null){
			this.entity = new Empresa();
		}else{
			this.entity = empresaService.obterEmpresaPorId(id);
		}
	}
	
	public String novo(){
		return "/pages/empresa/empresaForm?faces-redirect=true";
	}
	
	public String editar(){
		return "/pages/empresa/empresaForm?faces-redirect=true&id="+this.id;
	}
	
	public String salvar(){
		this.empresaService.salvarEmpresa(this.entity);
		return "/pages/empresa/empresaList?faces-redirect=true";
	}

	public Empresa getEntity() {
		return entity;
	}

	public void setEntity(Empresa entity) {
		this.entity = entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
