/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;

/**
 *
 * @author rodrigokuntzer
 */
public interface IEmpresaRepository extends IBaseRepository<Empresa, Long> {
	
}
