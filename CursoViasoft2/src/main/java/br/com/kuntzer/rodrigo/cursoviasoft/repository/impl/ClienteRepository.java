/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Cliente;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IClienteRepository;
import java.util.List;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rodrigokuntzer
 */
@Repository
public class ClienteRepository extends JPABaseRepository<Cliente, Long> implements IClienteRepository{

	@Override
	public List<Cliente> obterTodos() {
		Query consulta = entityManager.createQuery("select c from Cliente c join fetch c.estado order by c.id");
		return consulta.getResultList();
	}

	@Override
	public List<Cliente> obterClientesPesquisa(Cliente cliente) {
		StringBuilder hql = new StringBuilder();
		hql.append("select c from Cliente c join fetch c.estado e where 1 = 1 ");
		if(StringUtils.isNotEmpty(cliente.getNome())){
			hql.append(" and c.nome like :nome");
		}
		if(cliente.getTipoCliente() != null){
			hql.append(" and c.tipoCliente = :tipo");
		}
		if(cliente.getEstado() != null && StringUtils.isNotEmpty(cliente.getEstado().getSigla())){
			hql.append(" and e.sigla =:uf");
		}
		hql.append(" order by c.id");
		Query consulta = entityManager.createQuery(hql.toString());
		if(StringUtils.isNotEmpty(cliente.getNome())){
			consulta.setParameter("nome", "%"+cliente.getNome()+"%");
		}
		if(cliente.getTipoCliente() != null){
			consulta.setParameter("tipo", cliente.getTipoCliente());
		}
		if(cliente.getEstado() != null && StringUtils.isNotEmpty(cliente.getEstado().getSigla())){
			consulta.setParameter("uf", cliente.getEstado().getSigla());
		}
		return consulta.getResultList();
	}
	
}
