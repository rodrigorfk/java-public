/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Cliente;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IClienteService {

	List<Cliente> obterTodosClientes();
	Cliente obterClientePorId(Long id);
	Cliente salvarCliente(Cliente cliente);
	List<Cliente> obterClientesPesquisa(Cliente cliente);
}
