/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.entity;

/**
 *
 * @author rodrigokuntzer
 */
public enum TipoCliente {

	FISICA("F�sica"), JURIDICA("Juridica");
	
	private String description;

	private TipoCliente(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
	
	
}
