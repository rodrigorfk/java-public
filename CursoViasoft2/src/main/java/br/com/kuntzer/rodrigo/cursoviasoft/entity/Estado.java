/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.entity;

import javax.persistence.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author rodrigokuntzer
 */
@Entity
@Table(name="ESTADO")
@Cacheable
@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.READ_WRITE)
public class Estado extends BaseEntity<Long> {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(name="NOME_ESTADO",length=100, nullable=false)
	private String nome;
	
	@Column(name="UF", length=2, nullable=false)
	private String sigla;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	
}
