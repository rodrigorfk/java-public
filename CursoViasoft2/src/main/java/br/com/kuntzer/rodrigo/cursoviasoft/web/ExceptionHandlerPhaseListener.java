/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web;

import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.*;

/**
 *
 * @author rodrigokuntzer
 */
public class ExceptionHandlerPhaseListener implements PhaseListener {

	@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext context = event.getFacesContext();
		ExceptionHandler handler = context.getExceptionHandler();
		
		Iterator<ExceptionQueuedEvent> events = handler.getUnhandledExceptionQueuedEvents().iterator();
		while(events.hasNext()){
			ExceptionQueuedEvent exceptionEvent = events.next();
			ExceptionQueuedEventContext eventContext = exceptionEvent.getContext();
			Throwable ex = eventContext.getException();
			
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro na aplica��o: "+ex.getLocalizedMessage(), null));
		}
		
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.INVOKE_APPLICATION;
	}
	
}
