/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Cliente;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IClienteRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodrigokuntzer
 */
@Service("clienteService")
public class ClienteService implements IClienteService {

	@Autowired
	private IClienteRepository clienteRepository;
	
	@Override
	public List<Cliente> obterTodosClientes() {
		return clienteRepository.obterTodos();
	}

	@Override
	public Cliente obterClientePorId(Long id) {
		return clienteRepository.obterPorId(id);
	}

	@Override
	@Transactional
	public Cliente salvarCliente(Cliente cliente) {
		return clienteRepository.salvar(cliente);
	}

	@Override
	public List<Cliente> obterClientesPesquisa(Cliente cliente) {
		return clienteRepository.obterClientesPesquisa(cliente);
	}
	
}
