/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author rodrigokuntzer
 */
@Entity
@Table(name="USUARIO")
@org.hibernate.annotations.Cache(usage= CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Usuario extends BaseEntity<Long> {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	
	@Column(length=200, nullable=false)
	private String nome;
	
	@Column(length=200, nullable=false)
	private String email;
	
	@Column(length=30, nullable=false, unique=true)
	private String username;
	
	@Column(length=30, nullable=false)
	private String password;
	
	@ManyToMany(cascade={CascadeType.ALL}, fetch= FetchType.LAZY)
	@JoinTable(name="USUARIO_PERMISSOES",inverseJoinColumns=@JoinColumn(name="ID_PERFIL"), 
			uniqueConstraints = @UniqueConstraint(name = "UK_USUARIO_PERMISSOES",columnNames = {"ID_PERFIL", "USUARIO_ID"})
	)
	private List<UsuarioPerfil> perfis = new ArrayList<UsuarioPerfil>();
	
	@ManyToMany(cascade={CascadeType.ALL}, fetch= FetchType.LAZY)
	@JoinTable(name="USUARIO_EMPRESAS",inverseJoinColumns=@JoinColumn(name="ID_EMPRESA"), joinColumns=@JoinColumn(name="ID_USUARIO"),
			uniqueConstraints = @UniqueConstraint(name = "UK_USUARIO_EMPRESAS",columnNames = {"ID_EMPRESA", "ID_USUARIO"})
	)
	private List<Empresa> empresas = new ArrayList<Empresa>();

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<UsuarioPerfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<UsuarioPerfil> perfis) {
		this.perfis = perfis;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	
	
}
