/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.controller;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Estado;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IEmpresaService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author rodrigokuntzer
 */
@Controller
@Scope("view")
public class EstadoController implements Serializable {
	
	@Autowired
	private IEmpresaService empresaService;
	
	private List<Estado> listaEstados = new ArrayList<Estado>();

	private Estado estadoEdit;
	
	private LazyDataModel<Estado> lazyModel;
	
	public void listar(){
		this.listaEstados = empresaService.obterTodosEstados();
	}
	
	public void listarLazy(){
		this.lazyModel = new LazyDataModel<Estado>() {
			
			@Override
			public void setRowIndex(int rowIndex) {
				int pageSize = this.getPageSize();
				rowIndex = pageSize == 0 ? -1 : (rowIndex == -1 || pageSize == 0 ? rowIndex : (rowIndex % pageSize));
				super.setRowIndex(rowIndex);
			}
			
			@Override
			public List<Estado> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
				return EstadoController.this.empresaService.obterTodosEstadosPaginado(first, pageSize);
			}
		};
		this.lazyModel.setRowCount(empresaService.obterNumeroEstados().intValue());
	}
	
	public void salvar(){
		empresaService.salvarEstado(this.estadoEdit);
		
		if(this.lazyModel != null){
			this.listarLazy();
		}else{
			this.listar();
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Estado salvo com sucesso!", null));
	}
	
	public void novo(){
		this.estadoEdit = new Estado();
	}
	
	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public Estado getEstadoEdit() {
		return estadoEdit;
	}

	public void setEstadoEdit(Estado estadoEdit) {
		this.estadoEdit = estadoEdit;
	}

	public LazyDataModel<Estado> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Estado> lazyModel) {
		this.lazyModel = lazyModel;
	}
	
	
}
