/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Estado;

/**
 *
 * @author rodrigokuntzer
 */
public interface IEstadoRepository extends IBaseRepository<Estado, Long> {
	
}
