/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.Estado;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IEmpresaRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IEstadoRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IEmpresaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodrigokuntzer
 */
@Service
public class EmpresaService implements IEmpresaService {

	@Autowired
	private IEmpresaRepository empresaRepository;

	@Autowired
	private IEstadoRepository estadoRepository;
	
	@Override
	public List<Empresa> obterTodasEmpresas() {
		return empresaRepository.obterTodos();
	}

	@Override
	@Transactional
	public void salvarEmpresa(Empresa empresa) {
		empresaRepository.salvar(empresa);
	}

	@Override
	public Empresa obterEmpresaPorId(Long id) {
		return empresaRepository.obterPorId(id);
	}

	@Override
	public List<Estado> obterTodosEstados() {
		return estadoRepository.obterTodos();
	}

	@Override
	public Estado obterEstadoPorId(Long id) {
		return estadoRepository.obterPorId(id);
	}

	@Override
	@Transactional
	public void salvarEstado(Estado estado) {
		estadoRepository.salvar(estado);
	}

	@Override
	public List<Estado> obterTodosEstadosPaginado(Integer inicial, Integer resultados) {
		return estadoRepository.obterTodosPaginado(inicial, resultados);
	}

	@Override
	public Long obterNumeroEstados() {
		return estadoRepository.obterNumeroResultados();
	}
	
	
}
