/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.Usuario;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IUsuarioRepository extends IBaseRepository<Usuario, Long> {
	
	List<Empresa> obterEmpresasUsuario(String username);
	
	Usuario obterUsuarioPorUsername(String username);
	
	Usuario obterUsuarioPorUsernameAndEmpresa(String username, Long empresa);
}
