/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IMenuAplicacaoRepository extends IBaseRepository<MenuAplicacao, Long> {
	
	List<MenuAplicacao> obterMenusPrincipais();
}
