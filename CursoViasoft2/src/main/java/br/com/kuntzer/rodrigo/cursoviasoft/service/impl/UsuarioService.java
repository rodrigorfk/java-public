/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.Usuario;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IEmpresaRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IMenuAplicacaoRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IUsuarioRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IUsuarioService;
import br.com.kuntzer.rodrigo.cursoviasoft.web.security.CompanyUserDetailsService;
import br.com.kuntzer.rodrigo.cursoviasoft.web.security.UsuarioUserDetails;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author rodrigokuntzer
 */
@Service
public class UsuarioService implements IUsuarioService, CompanyUserDetailsService {

	@Autowired
	private IMenuAplicacaoRepository menuAplicacaoRepository;
	
	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	@Autowired
	private IEmpresaRepository empresaRepository;
	
	@Override
	public List<MenuAplicacao> obterMenusPrincipais() {
		return menuAplicacaoRepository.obterMenusPrincipais();
	}

	@Override
	public List<Empresa> obterEmpresasUsuario(String username) {
		return usuarioRepository.obterEmpresasUsuario(username);
	}

	@Override
	public UserDetails loadUserByUsernameAndCompany(String username, String company) {
		if(StringUtils.isNotEmpty(company) && NumberUtils.isNumber(company)){
			Long idempresa = Long.valueOf(company);
			Usuario usuario = usuarioRepository.obterUsuarioPorUsernameAndEmpresa(username, idempresa);
			if(usuario != null){
				Empresa empresa = empresaRepository.obterPorId(idempresa);
				return new UsuarioUserDetails(usuario, empresa);
			}
		}
		throw new UsernameNotFoundException("login.user.notfound");
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.obterUsuarioPorUsername(username);
		if(usuario != null){
			return new UsuarioUserDetails(usuario, null);
		}
		throw new UsernameNotFoundException("login.user.notfound");
	}
	
}
