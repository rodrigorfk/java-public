/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 *
 * @author rodrigokuntzer
 */
public class UsernamePasswordCompanyAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private Object company;
	
	public UsernamePasswordCompanyAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}

	public UsernamePasswordCompanyAuthenticationToken(Object principal, Object credentials, Object company) {
		this(principal, credentials);
		this.company = company;
	}

	public Object getCompany() {
		return company;
	}

	public void setCompany(Object company) {
		this.company = company;
	}
	
	
}
