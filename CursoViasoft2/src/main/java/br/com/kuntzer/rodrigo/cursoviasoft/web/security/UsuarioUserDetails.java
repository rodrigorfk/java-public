/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.security;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.Usuario;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.UsuarioPerfil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author rodrigokuntzer
 */
public class UsuarioUserDetails implements Serializable, UserDetails {

	private Usuario usuario;
	private Empresa empresa;
	private List<SimpleGrantedAuthority> authoritys = new ArrayList<SimpleGrantedAuthority>();

	public UsuarioUserDetails(Usuario usuario, Empresa empresa) {
		this.usuario = usuario;
		this.empresa = empresa;
		for(UsuarioPerfil perfil:usuario.getPerfis()){
			this.authoritys.add(new SimpleGrantedAuthority(perfil.getNome()));
		}
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authoritys;
	}

	@Override
	public String getPassword() {
		return usuario.getPassword();
	}

	@Override
	public String getUsername() {
		return usuario.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	
	
}
