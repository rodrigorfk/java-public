/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IUsuarioService extends Serializable {
	
	List<MenuAplicacao> obterMenusPrincipais();
	List<Empresa> obterEmpresasUsuario(String username);
}
