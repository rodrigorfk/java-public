/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.entity.Estado;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IEmpresaService extends Serializable {
	
	List<Empresa> obterTodasEmpresas();
	Empresa obterEmpresaPorId(Long id);
	void salvarEmpresa(Empresa empresa);
	
	List<Estado> obterTodosEstados();
	List<Estado> obterTodosEstadosPaginado(Integer inicial, Integer resultados);
	Estado obterEstadoPorId(Long id);
	void salvarEstado(Estado estado);
	Long obterNumeroEstados();
}
