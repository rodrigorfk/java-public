/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.controller;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IUsuarioService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

/**
 *
 * @author rodrigokuntzer
 */
@Controller
@Scope("request")
public class LoginController implements Serializable{
	
	private static final Logger _logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private IUsuarioService usuarioService;
	
	private List<Empresa> empresas = new ArrayList<Empresa>();
	private String username;

	public void handleUsernameChange(){
		if(StringUtils.isNotEmpty(username)){
			empresas = usuarioService.obterEmpresasUsuario(username);
		}
	}

	public void handleLogin(){
		this.handleLoginInternal();
	}
	
	public boolean handleLoginInternal(){
		try{
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			
			RequestDispatcher dispatcher = ((HttpServletRequest)context.getRequest()).getRequestDispatcher("/j_spring_security_check");
			dispatcher.forward((HttpServletRequest)context.getRequest(), (HttpServletResponse)context.getResponse());
			
			FacesContext.getCurrentInstance().responseComplete();
			
			SecurityContext securityContext = SecurityContextHolder.getContext();
			if(securityContext != null && securityContext.getAuthentication() != null && securityContext.getAuthentication().isAuthenticated()){
				return true;
			}
			
		}catch(Exception ex){
			_logger.error("Erro no login", ex);
		}
		return false;
	}
	
	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
