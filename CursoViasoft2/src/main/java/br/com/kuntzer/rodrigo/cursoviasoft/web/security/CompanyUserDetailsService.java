/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 *
 * @author rodrigokuntzer
 */
public interface CompanyUserDetailsService extends UserDetailsService {

	UserDetails loadUserByUsernameAndCompany(String username, String company);
}
