/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 *
 * @author rodrigokuntzer
 */
public class UsernamePasswordCompanyAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	public static final String SPRING_SECURITY_FORM_COMPANY_KEY = "j_company";
	
	private boolean postOnly = true;
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        String username = obtainUsername(request);
        String password = obtainPassword(request);
		String company  = obtainCompany(request);

        if (username == null) {
            username = "";
        }

        if (password == null) {
            password = "";
        }
		
		if (company == null) {
            company = "";
        }

        username = username.trim();

        UsernamePasswordCompanyAuthenticationToken authRequest = new UsernamePasswordCompanyAuthenticationToken(username, password, company);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);

        return this.getAuthenticationManager().authenticate(authRequest);
    }
	
	@Override
	public void setPostOnly(boolean postOnly) {
		super.setPostOnly(postOnly);
        this.postOnly = postOnly;
    }
	
	protected String obtainCompany(HttpServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_COMPANY_KEY);
    }
}
