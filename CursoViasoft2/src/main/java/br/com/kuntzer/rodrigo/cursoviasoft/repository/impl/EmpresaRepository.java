/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Empresa;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IEmpresaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rodrigokuntzer
 */
@Repository
public class EmpresaRepository extends JPABaseRepository<Empresa, Long> implements IEmpresaRepository {

	
}
