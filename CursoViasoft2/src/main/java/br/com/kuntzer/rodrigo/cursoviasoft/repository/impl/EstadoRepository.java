/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.Estado;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IEstadoRepository;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rodrigokuntzer
 */
@Repository
public class EstadoRepository extends JPABaseRepository<Estado, Long> implements IEstadoRepository{

	@Override
	public List<Estado> obterTodos() {
		Session session = (Session) entityManager.getDelegate();
		Query consulta = session.createQuery("from Estado e order by e.sigla");
		consulta.setCacheable(true).setCacheRegion("regiaoEstados");
		
		return consulta.list();
	}
	
}
