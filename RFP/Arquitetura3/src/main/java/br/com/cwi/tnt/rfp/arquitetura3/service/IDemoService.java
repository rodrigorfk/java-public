/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura3.service;

import br.com.cwi.tnt.rfp.arquitetura3.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura3.entity.SingleRowEntity;
import java.util.List;

/**
 *
 * @author kuntzer
 */
public interface IDemoService {
   
    SingleRowEntity obterSingleRowEntity();
    SingleRowEntity salvarSingleRowEntity(SingleRowEntity entity);
    
    List<GridEntity> obterTodosGridEntity();
    GridEntity salvarGridEntity(GridEntity entity);
    void excluirGridEntity(GridEntity entity);
}
