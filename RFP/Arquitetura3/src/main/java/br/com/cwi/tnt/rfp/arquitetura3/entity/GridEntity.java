/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura3.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 *
 * @author kuntzer
 */
@Entity
public class GridEntity extends AbstractPersistable<Long> {
    
    @Column(nullable=false, length=200)
    private String descricao;
    @Column(nullable=false, precision=10, scale=2)
    private BigDecimal valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    
}
