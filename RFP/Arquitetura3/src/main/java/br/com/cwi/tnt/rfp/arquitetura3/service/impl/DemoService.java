/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura3.service.impl;

import br.com.cwi.tnt.rfp.arquitetura3.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura3.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura3.repository.GridEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura3.repository.SingleRowEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura3.service.IDemoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kuntzer
 */
@Service
public class DemoService implements IDemoService {

    @Autowired
    private GridEntityDataRepository gridEntityDataRepository;
    
    @Autowired
    private SingleRowEntityDataRepository singleRowEntityDataRepository;
    
    @Override
    public SingleRowEntity obterSingleRowEntity() {
        List<SingleRowEntity> lista = singleRowEntityDataRepository.findAll();
        if(!lista.isEmpty()){
            return lista.get(0);
        }
        return new SingleRowEntity();
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED,readOnly=false)
    public SingleRowEntity salvarSingleRowEntity(SingleRowEntity entity) {
        return singleRowEntityDataRepository.save(entity);
    }

    @Override
    public List<GridEntity> obterTodosGridEntity() {
        return gridEntityDataRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED,readOnly=false)
    public GridEntity salvarGridEntity(GridEntity entity) {
        return gridEntityDataRepository.save(entity);
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED,readOnly=false)
    public void excluirGridEntity(GridEntity entity) {
        gridEntityDataRepository.delete(entity);
    }
    
}
