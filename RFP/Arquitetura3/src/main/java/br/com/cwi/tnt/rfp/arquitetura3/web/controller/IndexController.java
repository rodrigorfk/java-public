/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura3.web.controller;

import br.com.cwi.tnt.rfp.arquitetura3.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura3.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura3.service.IDemoService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author kuntzer
 */
@ManagedBean
@ViewScoped
public class IndexController implements Serializable {
    
    @ManagedProperty(value="#{demoService}")
    private IDemoService demoService;

    private SingleRowEntity singleRowEntity;
    
    private List<GridEntity> listaGrid = new ArrayList<GridEntity>();
    private GridEntity gridEdit;
    private CartesianChartModel categoryModel;
    private PieChartModel pieModel;  
    
    public void init(){
        this.singleRowEntity = demoService.obterSingleRowEntity();
        this.listaGrid = demoService.obterTodosGridEntity();
        this.createChartModel();
    }
    
    public void salvarSingleRow(){
        this.singleRowEntity = this.demoService.salvarSingleRowEntity(singleRowEntity);
    }
    
    public void novoGridEntity(){
        this.gridEdit = new GridEntity();
    }
    
    public void editarGridEntity(GridEntity entity){
        this.gridEdit = entity;
    }
    
    public void salvarEntity(){
        this.gridEdit = demoService.salvarGridEntity(gridEdit);
        this.listaGrid = demoService.obterTodosGridEntity();
        this.createChartModel();
    }
    
    public void excluirGridEntity(GridEntity entity){
        demoService.excluirGridEntity(entity);
        this.createChartModel();
    }
    
    public List<GridEntity> completeEntity(String query) {  
        List<GridEntity> suggestions = new ArrayList<GridEntity>();  
          
        List<GridEntity> lista = demoService.obterTodosGridEntity();
        for(GridEntity p : lista) {  
            if(p.getDescricao().toLowerCase().startsWith(query.toLowerCase())){
                suggestions.add(p);  
            }
        }  
          
        return suggestions;  
    } 
    
    private void createChartModel() {
        categoryModel = new CartesianChartModel();

        ChartSeries serie = new ChartSeries();
        List<GridEntity> lista = this.listaGrid;
        for(GridEntity p : lista) {  
            serie.set(p.getDescricao(), p.getValor());
        }
        categoryModel.addSeries(serie);
        
        pieModel = new PieChartModel();
        for(GridEntity p : lista) {  
            pieModel.set(p.getDescricao(), p.getValor());
        }
    }
    
    public String getDatatipFormat(){
        return "<span style=\"display:none;\">%s</span><span>%s</span>";
    }
    
    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public void setDemoService(IDemoService demoService) {
        this.demoService = demoService;
    }

    public List<GridEntity> getListaGrid() {
        return listaGrid;
    }

    public void setListaGrid(List<GridEntity> listaGrid) {
        this.listaGrid = listaGrid;
    }

    public GridEntity getGridEdit() {
        return gridEdit;
    }

    public void setGridEdit(GridEntity gridEdit) {
        this.gridEdit = gridEdit;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CartesianChartModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public PieChartModel getPieModel() {
        return pieModel;
    }

    public void setPieModel(PieChartModel pieModel) {
        this.pieModel = pieModel;
    }
    
    public SingleRowEntity getSingleRowEntity() {
        return singleRowEntity;
    }
    
    public void setSingleRowEntity(SingleRowEntity singleRowEntity) {
        this.singleRowEntity = singleRowEntity;
    }
    
    //</editor-fold>
    
    
}
