/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura3.repository;

import br.com.cwi.tnt.rfp.arquitetura3.entity.GridEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author kuntzer
 */
public interface GridEntityDataRepository extends JpaRepository<GridEntity, Long> {
    
}
