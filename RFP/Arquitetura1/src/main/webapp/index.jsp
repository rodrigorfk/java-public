<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>TNT-RFP: Arquitetura 1</title>
        <link type="text/css" rel="stylesheet" href="resources/css/reset.css" />
        <link type="text/css" rel="stylesheet" href="resources/css/ui-lightness/jquery-ui-1.8.23.css" />
        <link type="text/css" rel="stylesheet" href="resources/css/app.css" />
        <script type="text/javascript" src="resources/js/head.min.js"></script>
    </head>
    <body>
        <div>
            <img src="${pageContext.request.contextPath}/resources/image/28658.jpg" alt="logo"/>
        </div>
        <div class="ui-panel ui-widget ui-widget-content ui-corner-all ui-panel-full" style="display: none;">
            <div class="ui-panel-titlebar ui-widget-header ui-helper-clearfix ui-corner-all">
                <span class="ui-panel-title">TNT-RFP - Arquitetura 1</span>
            </div>
            <div class="ui-panel-content ui-widget-content">
                <div class="ui-accordion-app">
                    <h3><a href="#">Componentes Simples</a></h3>
                    <div class="app-simpleTab">
                        <div class="ui-panel-form ui-helper-clearfix">
                            <div class="ui-form-label">Campo de Texto:</div>
                            <div class="ui-form-field">
                                <input id="form1:texto1" type="text" value="" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" role="textbox" aria-disabled="false" aria-readonly="false" aria-multiline="false">
                            </div>

                            <div class="ui-form-label">Combo:</div>
                            <div class="ui-form-field">
                                <select id="form1:combobox">
                                    <option value="">Select one...</option>
                                    <option value="ActionScript">ActionScript</option>
                                    <option value="AppleScript">AppleScript</option>
                                    <option value="Asp">Asp</option>
                                    <option value="BASIC">BASIC</option>
                                    <option value="C">C</option>
                                    <option value="C++">C++</option>
                                    <option value="Clojure">Clojure</option>
                                    <option value="COBOL">COBOL</option>
                                    <option value="ColdFusion">ColdFusion</option>
                                    <option value="Erlang">Erlang</option>
                                    <option value="Fortran">Fortran</option>
                                    <option value="Groovy">Groovy</option>
                                    <option value="Haskell">Haskell</option>
                                    <option value="Java">Java</option>
                                    <option value="JavaScript">JavaScript</option>
                                    <option value="Lisp">Lisp</option>
                                    <option value="Perl">Perl</option>
                                    <option value="PHP">PHP</option>
                                    <option value="Python">Python</option>
                                    <option value="Ruby">Ruby</option>
                                    <option value="Scala">Scala</option>
                                    <option value="Scheme">Scheme</option>
                                </select>
                            </div>

                            <div class="ui-form-label">Data:</div>
                            <div class="ui-form-field">
                                <input id="form1:data1" type="text" readonly>
                            </div>
                            
                            <div class="ui-form-label">Autocomplete:</div>
                            <div class="ui-form-field">
                                <input id="form1:autocomplete" type="text">
                            </div>

                            <div class="ui-form-footer">
                                <button class="btnSalvar">Salvar</button>
                            </div>
                        </div>
                    </div>
                    <h3><a href="#">Grid</a></h3>
                    <div class="app-gridTab">

                    </div>
                    <h3><a href="#">Gr�ficos</a></h3>
                    <div class="app-chartTab">
                        <div class="chartBar"></div>
                        <div class="chartPie"></div>
                    </div>
                </div>
            </div>
            <div class="ui-panel-footer ui-widget-content">Running jquery 1.8.0, jqueryui 1.8.23, canjs 1.0.7, JAX-RS 1.1, CDI 1.0, EclipseLink 2.3.2 on Glassfish 3.1.2</div>
        </div>

        <script type="text/javascript">
            var contextPath = "${pageContext.request.contextPath}", applicationRESTPath = contextPath+"/rest";
            head.js("resources/js/jquery-1.8.0.min.js","resources/js/jquery-ui-1.8.23.min.js","resources/js/json2.js",
            "resources/js/can.jquery-1.0.7.min.js",//"http://cloud.github.com/downloads/jupiterjs/canjs/can.jquery-1.0.7.js",
            "resources/js/highcharts.js",
            "resources/js/app.model.js","resources/js/app.js");
        </script>
    </body>
</html>
