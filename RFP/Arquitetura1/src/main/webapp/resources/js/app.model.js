var SingleRowEntity = can.Model({
    findOne  : 'GET '+applicationRESTPath+'/index/singleRow',
    createUrl: 'POST '+applicationRESTPath+'/index/singleRow',
    create: function(attrs){
		var def = new can.Deferred();
		var json_data = JSON.stringify(attrs);
        
        var parts = this.createUrl.split(" ");
        var ajaxOb = {
            url : parts.pop()
        };
        if(parts.length){
            ajaxOb.type = parts.pop();
        }
        
        $.ajax($.extend({
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            type: "POST",
            data: json_data,
            success: function(data){
                def.resolve({id : data.id});
            }
        }, ajaxOb));
		return def;
	},
	update: function(id, attrs){
		return this.create(attrs);
	}
}, {});

var GridEntity = can.Model({
    findAll  : 'GET '+applicationRESTPath+'/index/grid/all',
    createUrl: 'POST '+applicationRESTPath+'/index/grid',
    destroy :  'DELETE '+applicationRESTPath+'/index/grid/{id}',
    create: function(attrs){
		var def = new can.Deferred();
		var json_data = JSON.stringify(attrs);
        
        var parts = this.createUrl.split(" ");
        var ajaxOb = {
            url : parts.pop()
        };
        if(parts.length){
            ajaxOb.type = parts.pop();
        }
        
        $.ajax($.extend({
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            type: "POST",
            data: json_data,
            success: function(data){
                def.resolve({id : data.id});
            }
        }, ajaxOb));
		return def;
	},
	update: function(id, attrs){
		return this.create(attrs);
	}
}, {});

var SimpleTabController = can.Control({
    'init' : function(element) {
        var self = this;
        $("#form1\\:combobox").combobox();
    
        $("#form1\\:data1").datepicker({
            showOn: "button",
            dateFormat: "dd/mm/yy"
        });
        element.find(".btnSalvar").button({
            icons: {
                primary: "ui-icon-disk"
            }
        });
        element.find(".ui-datepicker-trigger").button({
            icons: {
                primary: "ui-icon-calendar"
            },
            text: false
        });
        $( "#form1\\:autocomplete" ).autocomplete({
			source: applicationRESTPath+'/index/grid/suggest',
			minLength: 2,
			select: function( event, ui ) {
				$(this).val( ui.item.descricao );
                self.onSuggestSelected(ui.item);
                $(this).data("uiItem",ui.item.descricao);
				return false;
			}
		}).bind("blur",function(){
            $(this).val($(this).data("uiItem"));        
        }).data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.descricao + "</a>" )
				.appendTo( ul );
		};
        
        SingleRowEntity.findOne( {},function( singleRow ) {
            $("#form1\\:texto1").val(singleRow.attr("texto1"));
            if(singleRow.attr("combo1") != null){
                $("#form1\\:combobox").combobox("setSelected",singleRow.attr("combo1"));
            }
            $("#form1\\:data1").datepicker("setDate",singleRow.attr("dateDate"));
            var autocomplete = singleRow.attr('autocomplete') ? singleRow.autocomplete.attr("descricao") : "";
            $("#form1\\:autocomplete").val(autocomplete).data("uiItem",autocomplete);
            element.data("row",singleRow);
        });
        
        
    },
    '#form1\\:texto1 focusin': function(el, ev){
        $(el).addClass("ui-state-focus");
    },
    '#form1\\:texto1 focusout': function(el, ev){
        $(el).removeClass("ui-state-focus");
    },
    '.btnSalvar click' : function(el, ev){
        var self = this;
        var singleRow = self.element.data("row")
        singleRow.attr("texto1",$("#form1\\:texto1").val());
        singleRow.attr("combo1",$("#form1\\:combobox").combobox("getValue"));
        singleRow.attr("dateDate",$("#form1\\:data1").datepicker("getDate"));
        singleRow.save();
    },
    onSuggestSelected: function(item){
        var self = this;
        var singleRow = self.element.data("row")
        singleRow.attr("autocomplete",new GridEntity(item));
    }
});

var GridTabController = can.Control({
    'init' : function(element) {
        var self = this;
        
        GridEntity.findAll({}, function(lista){
            var model = new can.Observe({
                'lista': lista,
                'item': new GridEntity({})
            });
            element.data("model",model);
            element.html( can.view( 'pages/grid.ejs', model ) );
            element.find(".btnNovo").button({
                icons: {
                    primary: "ui-icon-document"
                }
            }).end().find(".btnSalvar").button({
                icons: {
                    primary: "ui-icon-disk"
                }
            }).click(function(){
                self.salvarAction(this);
            });
            
            $("#ui-app-dialog").dialog({
                height: 300,
                width: 700,
                modal: true,
                autoOpen: false,
                resizable: false
            });
            self.options.deferedGrid.resolve(model);
        });
    },
    '.btnNovo click' : function(el, ev){
        var model = this.element.data("model");
        model.attr("item",new GridEntity({}));
        $("#ui-app-dialog").dialog("open");
    },
    '.btnEditar click' : function(el, ev){
        var model = this.element.data("model");
        model.attr("item",el.data("item"));
        $("#ui-app-dialog").dialog("open");
    },
    '.btnExcluir click' : function(el, ev){
        var item = el.data("item");
        item.destroy();
    },
    salvarAction: function(el){
        var model = this.element.data("model");
        var item = model.attr("item");
        item.attr("descricao", $("#form2\\:descricao").val());
        item.attr("valor", parseFloat($("#form2\\:valor").val()));
        item.save(function(){
            $("#ui-app-dialog").dialog("close");
        });
    },
    '{GridEntity} created' : function(list, ev, item){
        var model = this.element.data("model");
        var lista = model.attr("lista");
		lista.push(item);
	}
});

var ChartTabController = can.Control({
    'init' : function(element) {
        var self = this;
        
        self.options.initiated = false;
        self.options.deferedGrid.then(function(model){
            element.data("model",model);
        });
    },
    buildCharts: function(){
        var self = this;
        var model = self.element.data("model");
        
        var lista = model.attr("lista");
        var categoriasData = [], valuesData = [];
        for(var i=0; i<lista.length; i++){
            categoriasData.push(lista[i].descricao);
            valuesData.push(lista[i].valor);
        }
        
        self.element.find(".chartBar").empty().end().find(".chartPie").empty();
        new Highcharts.Chart({
            chart: {
                renderTo: self.element.find(".chartBar")[0],
                type: 'column'
            },
            title: {
                text: 'Gr�fico em Barra'
            },
            xAxis: {
                categories: categoriasData
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Valor (R$)'
                }
            },
            legend : {
                enabled: false
            },
            tooltip: {
                formatter: function() {
                    return 'R$ '+ this.y;
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                data: valuesData
            }]
        });
        
        valuesData = [];
        for(i=0; i<lista.length; i++){
            valuesData.push({
                name: lista[i].descricao,
                y: lista[i].valor
            });
        }
        new Highcharts.Chart({
            chart: {
                renderTo: self.element.find(".chartPie")[0],
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Gr�fico em Pizza'
            },
            tooltip: {
        	    pointFormat: '<b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return roundNumber(this.point.percentage,2)+" %";
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Valor',
                data: valuesData
            }]
        });
        
    },
    active: function(){
        var self = this;
        if(!self.options.initiated){
            self.options.initiated = true;
            self.buildCharts();
        }
    },
    '{GridEntity} created' : function(list, ev, item){
        this.prepareToRedraw();
	},
    '{GridEntity} destroyed' : function(list, ev, item){
        this.prepareToRedraw();
	},
    '{GridEntity} updated' : function(list, ev, item){
        this.prepareToRedraw();
	},
    prepareToRedraw: function(){
        this.element.find(".chartBar").empty();
        this.options.initiated = false;
    }
});

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}