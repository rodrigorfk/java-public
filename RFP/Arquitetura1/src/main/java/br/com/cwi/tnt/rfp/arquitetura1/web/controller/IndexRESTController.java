/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.web.controller;

import br.com.cwi.tnt.rfp.arquitetura1.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura1.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura1.service.IDemoService;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author kuntzer
 */
@Path("index")
@RequestScoped
public class IndexRESTController {
    
    @Inject
    private IDemoService demoService;
    
    @GET
    @Path("singleRow")
    @Produces(MediaType.APPLICATION_JSON)
    public SingleRowEntity obterSingleRowEntity(){
        return demoService.obterSingleRowEntity();
    }
    
    @POST
    @Path("singleRow")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response salvarSingleRowEntity(SingleRowEntity entity){
        demoService.salvarSingleRowEntity(entity);
        
        return Response.status(201).entity(entity).build();
    }
    
    @GET
    @Path("grid/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GridEntity> obterTodosGridEntity(){
        return demoService.obterTodosGridEntity();
    }
    
    @POST
    @Path("grid")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response salvarGridEntity(GridEntity entity){
        demoService.salvarGridEntity(entity);
        return Response.status(201).entity(entity).build();
    }
    
    @GET
    @Path("grid/suggest")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GridEntity> obterSuggestGridEntity(@QueryParam("term") String query){
        List<GridEntity> suggestions = new ArrayList<GridEntity>();  
          
        List<GridEntity> lista = demoService.obterTodosGridEntity();
        for(GridEntity p : lista) {  
            if(p.getDescricao().toLowerCase().startsWith(query.toLowerCase())){
                suggestions.add(p);  
            }
        }  
          
        return suggestions;  
    }
    
    @DELETE
    @Path("grid/{id}")
	@Produces(MediaType.APPLICATION_JSON)
    public Response excluirGridEntity(@PathParam("id") Long id){
        GridEntity entity = demoService.obterGridEntityPorId(id);
        if(entity != null){
            demoService.excluirGridEntity(entity);
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    //<editor-fold defaultstate="collapsed" desc="setters">
    public void setDemoService(IDemoService demoService) {
        this.demoService = demoService;
    }
    //</editor-fold>
    
    
}
