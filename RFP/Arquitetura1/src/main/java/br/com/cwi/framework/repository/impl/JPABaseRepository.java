/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.framework.repository.impl;

import br.com.cwi.framework.entity.BaseEntity;
import br.com.cwi.framework.repository.IBaseRepository;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author kuntzer
 */
public abstract class JPABaseRepository<T extends BaseEntity<PK>, PK extends Serializable> implements IBaseRepository<T, PK> {

    @PersistenceContext
    protected EntityManager entityManager;
    private Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public JPABaseRepository() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public List<T> obterTodos() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(persistentClass);

        return entityManager.createQuery(
            query.select(
               query.from(persistentClass)
            )
        ).getResultList();
    }

    @Override
    public List<T> obterTodosPaginado(Integer inicial, Integer resultados) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(persistentClass);

        return entityManager.createQuery(
            query.select(
               query.from(persistentClass)
            )
        ).setFirstResult(inicial)
         .setMaxResults(inicial + resultados)
         .getResultList();
    }

    @Override
    public T salvar(T entity) {
        if (entity.getId() == null) {
            entityManager.persist(entity);
        }
        return entityManager.merge(entity);
    }

    @Override
    public T obterPorId(PK id) {
        return entityManager.find(persistentClass, id);
    }

    @Override
    public Long obterNumeroResultados() {
        return entityManager.createQuery("select count(c) from " + persistentClass.getSimpleName() + " c", Long.class)
                .getSingleResult();
    }

    @Override
    public void excluir(T entity) {
        if(entity.getId() != null){
            entity = entityManager.merge(entity);
            entityManager.remove(entity);
        }
    }
    
    
}
