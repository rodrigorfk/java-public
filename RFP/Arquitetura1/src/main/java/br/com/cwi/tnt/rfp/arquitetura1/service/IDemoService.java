/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.service;

import br.com.cwi.tnt.rfp.arquitetura1.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura1.entity.SingleRowEntity;
import java.util.List;

/**
 *
 * @author kuntzer
 */
public interface IDemoService {
   
    SingleRowEntity obterSingleRowEntity();
    SingleRowEntity salvarSingleRowEntity(SingleRowEntity entity);

    GridEntity obterGridEntityPorId(Long id);
    List<GridEntity> obterTodosGridEntity();
    GridEntity salvarGridEntity(GridEntity entity);
    void excluirGridEntity(GridEntity entity);
}
