/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.service.impl;

import br.com.cwi.tnt.rfp.arquitetura1.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura1.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura1.repository.GridEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura1.repository.SingleRowEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura1.service.IDemoService;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author kuntzer
 */
@Named
@Stateless
public class DemoService implements IDemoService {

    @Inject
    private GridEntityDataRepository gridEntityDataRepository;
    
    @Inject
    private SingleRowEntityDataRepository singleRowEntityDataRepository;
    
    @Override
    public SingleRowEntity obterSingleRowEntity() {
        List<SingleRowEntity> lista = singleRowEntityDataRepository.obterTodos();
        if(!lista.isEmpty()){
            return lista.get(0);
        }
        return new SingleRowEntity();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SingleRowEntity salvarSingleRowEntity(SingleRowEntity entity) {
        return singleRowEntityDataRepository.salvar(entity);
    }
    
    @Override
    public GridEntity obterGridEntityPorId(Long id) {
        return gridEntityDataRepository.obterPorId(id);
    }

    @Override
    public List<GridEntity> obterTodosGridEntity() {
        return gridEntityDataRepository.obterTodos();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GridEntity salvarGridEntity(GridEntity entity) {
        return gridEntityDataRepository.salvar(entity);
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void excluirGridEntity(GridEntity entity) {
        gridEntityDataRepository.excluir(entity);
    }

    //<editor-fold defaultstate="collapsed" desc="setters">
    public void setGridEntityDataRepository(GridEntityDataRepository gridEntityDataRepository) {
        this.gridEntityDataRepository = gridEntityDataRepository;
    }
    
    public void setSingleRowEntityDataRepository(SingleRowEntityDataRepository singleRowEntityDataRepository) {
        this.singleRowEntityDataRepository = singleRowEntityDataRepository;
    }
    //</editor-fold>

    
}
