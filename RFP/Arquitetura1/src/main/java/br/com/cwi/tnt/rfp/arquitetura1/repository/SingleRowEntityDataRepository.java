/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.repository;

import br.com.cwi.framework.repository.impl.JPABaseRepository;
import br.com.cwi.tnt.rfp.arquitetura1.entity.SingleRowEntity;
import javax.inject.Named;

/**
 *
 * @author kuntzer
 */
@Named
public class SingleRowEntityDataRepository extends JPABaseRepository<SingleRowEntity, Long> {
    
}
