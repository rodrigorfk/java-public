/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.framework.repository;

import br.com.cwi.framework.entity.BaseEntity;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author kuntzer
 */
public interface IBaseRepository<T extends BaseEntity<PK>, PK extends Serializable> {

    List<T> obterTodos();
    List<T> obterTodosPaginado(Integer inicial, Integer resultados);
    T salvar(T entity);
    T obterPorId(PK id);
    Long obterNumeroResultados();
    void excluir(T entity);
}
