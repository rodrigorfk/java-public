/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.entity;

import br.com.cwi.framework.entity.BaseEntity;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author kuntzer
 */
@Entity
public class GridEntity extends BaseEntity<Long> {
    
    @Column(nullable=false, length=200)
    private String descricao;
    @Column(nullable=false, precision=10, scale=2)
    private BigDecimal valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    
}
