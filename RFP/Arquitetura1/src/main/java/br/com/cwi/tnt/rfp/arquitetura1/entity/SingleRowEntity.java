/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura1.entity;

import br.com.cwi.framework.entity.BaseEntity;
import br.com.cwi.framework.utils.json.DateJsonSerializer;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author kuntzer
 */
@Entity
public class SingleRowEntity extends BaseEntity<Long> {
    
    @Column(length=200)
    private String texto1;
    @Column(length=200)
    private String combo1;
    @ManyToOne
    private GridEntity autocomplete;
    
    @JsonSerialize(using=DateJsonSerializer.class)
    @Column @Temporal(TemporalType.DATE)
    private Date dateDate;
    @Column @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public String getCombo1() {
        return combo1;
    }

    public void setCombo1(String combo1) {
        this.combo1 = combo1;
    }

    public GridEntity getAutocomplete() {
        return autocomplete;
    }

    public void setAutocomplete(GridEntity autocomplete) {
        this.autocomplete = autocomplete;
    }

    public Date getDateDate() {
        return dateDate;
    }

    public void setDateDate(Date dateDate) {
        this.dateDate = dateDate;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    
    
}
