(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {
            var self = this,
            select = this.element.hide(),
            selected = select.children( ":selected" ),
            value = selected.val() ? selected.text() : "",
            wrapper = this.wrapper = $( "<span>" )
            .addClass( "ui-combobox" )
            .insertAfter( select );

            var input = this.input = $( "<input>" )
            .appendTo( wrapper )
            .val( value )
            .addClass( "ui-state-default ui-combobox-input" )
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                    response( select.children( "option" ).map(function() {
                        var text = $( this ).text();
                        if ( this.value && ( !request.term || matcher.test(text) ) )
                            return {
                                label: text.replace(
                                    new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(request.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>" ),
                                value: text,
                                option: this
                            };
                    }) );
                },
                select: function( event, ui ) {
                    ui.item.option.selected = true;
                    self._trigger( "selected", event, {
                        item: ui.item.option
                    });
                },
                change: function( event, ui ) {
                    if ( !ui.item ) {
                        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                        valid = false;
                        select.children( "option" ).each(function() {
                            if ( $( this ).text().match( matcher ) ) {
                                this.selected = valid = true;
                                return false;
                            }
                        });
                        if ( !valid ) {
                            // remove invalid value, as it didn't match anything
                            $( this ).val( "" );
                            select.val( "" );
                            input.data( "autocomplete" ).term = "";
                            return false;
                        }
                    }
                }
            })
            .addClass( "ui-widget ui-widget-content ui-corner-left" );

            input.data( "autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };

            $( "<a>" )
            .attr( "tabIndex", -1 )
            .attr( "title", "Show All Items" )
            .appendTo( wrapper )
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass( "ui-corner-all" )
            .addClass( "ui-corner-right ui-combobox-toggle" )
            .click(function() {
                // close if already visible
                if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                    input.autocomplete( "close" );
                    return;
                }

                // work around a bug (likely same cause as #5265)
                $( this ).blur();

                // pass empty string as value to search for, displaying all results
                input.autocomplete( "search", "" );
                input.focus();
            });
        },
        setSelected: function(value){
            var option = this.element.find('option[value="'+value+'"]');
            if(option.size() == 0){
                this.element.find('option').each(function(){
                    if($(this).text().toUpperCase() == value.toUpperCase()){
                        option = $(this);
                        value = $(this).val();
                        return false;
                    }
                });
            }
            if(option.size() > 0){
                this.element.find( "option" ).removeAttr("selected");
                option.attr("selected","selected");
                this.input.val(option.text());
            }
        },
        getValue: function(){
            var option = this.element.find('option:selected');
            if(option.size() > 0){
                return option.attr("value");
            }
            return null;
        },
        destroy: function() {
            this.wrapper.remove();
            this.element.show();
            $.Widget.prototype.destroy.call( this );
        }
    });
})( jQuery );

$(document).ready(function(){
    $(".ui-accordion-app").accordion({
        autoHeight: false,
        change: function(event, ui){
            var active = $(this).accordion( "option", "active" );
            if(active == 2){
                $(".app-chartTab").trigger("active");
            }
        }
    });
    
    //inicio simpletab
    $("#form1\\:combobox").combobox();
    
    $("#form1\\:data1").datepicker({
        showOn: "button",
        dateFormat: "dd/mm/yy"
    });
    $(".app-simpleTab .ui-datepicker-trigger").button({
        icons: {
            primary: "ui-icon-calendar"
        },
        text: false
    });
    $(".app-simpleTab .btnSalvar").button({
        icons: {
            primary: "ui-icon-disk"
        }
    }).click(function(){
        var params = {
            'id': $("#form1\\:id").val(),
            'texto1': $("#form1\\:texto1").val(),
            'combo1': $("#form1\\:combobox").combobox("getValue"),
            'dateDate': $("#form1\\:data1").datepicker("getDate"),
            'autocomplete': $("#form1\\:autocompleteid").val()
        };
        var json_data = JSON.stringify(params);
        $.ajax( contextPath+"/index/singleRow", {
            data: json_data,
            dataType: 'json',
            type: "POST",
            contentType: "application/json",
            success: function(data){
                $("#form1\\:id").val(data.id);
            }
        });
    });
    $( "#form1\\:autocomplete" ).autocomplete({
        source: contextPath+'/index/grid/suggest',
        minLength: 2,
        select: function( event, ui ) {
            $(this).val( ui.item.descricao );
            $("#form1\\:autocompleteid").val(ui.item.id);
            $(this).data("uiItem",ui.item.descricao);
            return false;
        }
    }).bind("focusout",function(){
        $(this).val($(this).data("uiItem"));        
    }).data("uiItem",$("#form1\\:autocomplete").val()).data( "autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.descricao + "</a>" )
            .appendTo( ul );
    };
    //fim simpletab
    
    //inicio grid
    $(".app-gridTab .btnNovo").button({
        icons: {
            primary: "ui-icon-document"
        }
    }).click(function(){
        $("#form2\\:codigo").val("");
        $("#form2\\:descricao").val("");
        $("#form2\\:valor").val("");
        $("#ui-app-dialog").dialog("open");
    });
    
    $(".app-gridTab .btnSalvar").button({
        icons: {
            primary: "ui-icon-disk"
        }
    }).click(function(){
        var params = {
            'id': $("#form2\\:codigo").val(),
            'descricao': $("#form2\\:descricao").val(),
            'valor': $("#form2\\:valor").val()
        };
        var json_data = JSON.stringify(params);
        $.ajax( contextPath+"/index/grid", {
            data: json_data,
            dataType: 'json',
            type: "POST",
            contentType: "application/json",
            success: function(data){
                if(params.id == null || params.id == ""){
                    var html = '<tr class="ui-widget-content ui-datatable-even" data-id="'+data.id+'">';
                        html += '    <td role="gridcell"><div class="ui-dt-c">'+data.id+'</div></td>';
                        html += '    <td role="gridcell"><div class="ui-dt-c">'+params.descricao+'</div></td>';
                        html += '    <td role="gridcell"><div class="ui-dt-c">'+params.valor+'</div></td>';
                        html += '    <td role="gridcell"><div class="ui-dt-c">';
                        html += '            <button class="btnEditar">Editar</button>';
                        html += '            <button class="btnExcluir">Excluir</button>';
                        html += '        </div>';
                        html += '    </td>';
                        html += '</tr>";';
                    var tr = $(html).appendTo($(".app-gridTab .ui-datatable table tbody"));
                    configEventsTrGrid(tr);
                    $(".app-gridTab .ui-datatable table tbody tr.ui-datatable-empty-message").remove();
                }else{
                    $(".app-gridTab .ui-datatable table tbody tr").each(function(){
                        var $tr = $(this);
                        if($tr.attr("data-id") == params.id){
                            $tr.find("td:eq(1) div").html(params.descricao);
                            $tr.find("td:eq(2) div").html(params.valor);
                        }
                    });
                }
                $(".app-chartTab").trigger("clear");
                $("#ui-app-dialog").dialog("close");
            }
        });
    });
    
    $("#ui-app-dialog").dialog({
        height: 300,
        width: 700,
        modal: true,
        autoOpen: false,
        resizable: false
    });
    $(".app-gridTab .ui-datatable table tbody tr").each(function(){
        configEventsTrGrid($(this));
    });
    
    //fim grid
    
    //inicio charts
    $.jqplot.config.enablePlugins = true;
    $(".app-chartTab").data("initiated",false).bind("active",function(){
        var $this = $(this);
        var initiated = $this.data("initiated");
        if(!initiated){
            $this.data("initiated",true);
            $("#chartBar").empty();
            $("#chartPie").empty();
            
            $.ajax( contextPath+"/index/grid/all", {
                dataType: 'json',
                type: "GET",
                contentType: "application/json",
                success: function(data){
                    var s1 = [], ticks = [];
                    for(var i=0; i<data.length; i++){
                        ticks.push(data[i].descricao);
                        s1.push(data[i].valor);
                    }
                    
                    $.jqplot("chartBar", [s1], {
                        animate: true,
                        animateReplot: true,
                        title: {
                            text: 'Gr�fico em Barra'
                        },
                        seriesDefaults:{
                            renderer:$.jqplot.BarRenderer,
                            pointLabels: { show: true },
                            rendererOptions: {
                                animation: {
                                    speed: 1500
                                },
                                barWidth: 45,
                                barPadding: -15,
                                barMargin: 0,
                                highlightMouseOver: true
                            }
                        },
                        axes: {
                            xaxis: {
                                renderer: $.jqplot.CategoryAxisRenderer,
                                ticks: ticks
                            }
                        },
                        highlighter: { show: false }
                    });
                    
                    s1 = [];
                    for(i=0; i<data.length; i++){
                        s1.push([data[i].descricao,data[i].valor]);
                    }
                    
                    $.jqplot('chartPie', [s1], {
                        animate: true,
                        animateReplot: true,
                        title: {
                            text: 'Gr�fico em Pizza'
                        },
                        seriesDefaults:{ 
                            renderer:$.jqplot.PieRenderer, 
                            trendline:{ show: true },
                            rendererOptions: {
                                showDataLabels: true,
                                highlightMouseOver: true
                            }
                        },
                        legend:{ show: true }    
                    });
                }
            });

        }
    }).bind("clear",function(){
        $(this).data("initiated",false);
        $("#chartBar").empty();
        $("#chartPie").empty();
    });
    
    
    
    //fim charts
    
    $(".ui-panel-full").fadeIn("slow");
});

function configEventsTrGrid(elTr) {
    elTr.find(".btnEditar").button({
        icons: {primary: "ui-icon-pencil"},text: false
    }).click(function(){
        var tr = $(this).closest("tr");
        $("#form2\\:codigo").val(tr.find("td:eq(0) div").text());
        $("#form2\\:descricao").val(tr.find("td:eq(1) div").text());
        $("#form2\\:valor").val(tr.find("td:eq(2) div").text());
        $("#ui-app-dialog").dialog("open");
    });
    
    elTr.find(".btnExcluir").button({
        icons: {primary: "ui-icon-close"},text: false
    }).click(function(){
        var tr = $(this).closest("tr");
        var id = tr.attr("data-id");
        $.ajax( contextPath+"/index/grid/"+id, {
            dataType: 'json',
            type: "DELETE",
            contentType: "application/json",
            success: function(data){
                tr.remove();
                $(".app-chartTab").trigger("clear");
            }
        });
    });
}