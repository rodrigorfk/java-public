<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>TNT-RFP: Arquitetura 2</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/reset.css" />
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ui-lightness/jquery-ui-1.8.23.css" />
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" />
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.jqplot.min.css" />
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/head.min.js"></script>
    </head>
    <body>
        <div>
            <img src="${pageContext.request.contextPath}/resources/image/28658.jpg" alt="logo"/>
        </div>
        <div class="ui-panel ui-widget ui-widget-content ui-corner-all ui-panel-full" style="display: none;">
            <div class="ui-panel-titlebar ui-widget-header ui-helper-clearfix ui-corner-all">
                <span class="ui-panel-title">TNT-RFP - Arquitetura 1</span>
            </div>
            <div class="ui-panel-content ui-widget-content">
                <div class="ui-accordion-app">
                    <h3><a href="#">Componentes Simples</a></h3>
                    <div class="app-simpleTab">
                        <div class="ui-panel-form ui-helper-clearfix">
                            <div class="ui-form-label">Campo de Texto:</div>
                            <div class="ui-form-field">
                                <input id="form1:id" type="hidden" value="${singleRow.id}">
                                <input id="form1:texto1" type="text" value="${singleRow.texto1}" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" role="textbox" aria-disabled="false" aria-readonly="false" aria-multiline="false">
                            </div>

                            <div class="ui-form-label">Combo:</div>
                            <div class="ui-form-field">
                                <select id="form1:combobox">
                                    <c:forEach var="opcao" items="${opcaoCombos}">
                                        <option value="${opcao.value}" <c:if test="${opcao.selected}">selected="selected"</c:if>>${opcao.label}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="ui-form-label">Data:</div>
                            <div class="ui-form-field">
                                <fmt:formatDate var="data" value="${singleRow.dateDate}" pattern="dd/MM/yyyy"/>
                                <input id="form1:data1" type="text" value="${data}" readonly>
                            </div>
                            
                            <div class="ui-form-label">Autocomplete:</div>
                            <div class="ui-form-field">
                                <input id="form1:autocompleteid" type="hidden" value="${singleRow.autocomplete}">
                                <input id="form1:autocomplete" type="text" value="${singleRow.descricaoAutocomplete}">
                            </div>

                            <div class="ui-form-footer">
                                <button class="btnSalvar">Salvar</button>
                            </div>
                        </div>
                    </div>
                    <h3><a href="#">Grid</a></h3>
                    <div class="app-gridTab">
                        <div class="ui-panel ui-widget ui-widget-content ui-corner-all">
                            <div class="ui-panel-content ui-widget-content">
                                <div class="ui-datatable ui-widget">
                                    <table role="grid">
                                        <thead>
                                            <tr>
                                                <th class="ui-widget-header"><div class="ui-dt-c"><span>C&oacute;digo</span></div></th>
                                                <th class="ui-widget-header"><div class="ui-dt-c"><span>Descri&ccedil;&atilde;o</span></div></th>
                                                <th class="ui-widget-header"><div class="ui-dt-c"><span>Valor</span></div></th>
                                                <th class="ui-widget-header"><div class="ui-dt-c"><span></span></div></th>
                                            </tr>
                                        </thead>
                                        <tbody class="ui-datatable-data ui-widget-content">
                                            <c:forEach var="item" items="${grid}">
                                                <tr class="ui-widget-content ui-datatable-even" data-id="${item.id}">
                                                    <td role="gridcell"><div class="ui-dt-c">${item.id}</div></td>
                                                    <td role="gridcell"><div class="ui-dt-c">${item.descricao}</div></td>
                                                    <td role="gridcell"><div class="ui-dt-c">${item.valor}</div></td>
                                                    <td role="gridcell"><div class="ui-dt-c">
                                                            <button class="btnEditar" >Editar</button>
                                                            <button class="btnExcluir">Excluir</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            <c:if test="${empty grid}">
                                                <tr class="ui-widget-content ui-datatable-empty-message"><td colspan="4"><div class="ui-dt-c">N�o existem registros...</div></td></tr>
                                            </c:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="ui-panel-footer ui-widget-content">
                                <button class="btnNovo">Novo</button>
                            </div>
                        </div>
    
                        <div style="display: none;" id="ui-app-dialog" title="Novo Registro">
                            <div class="ui-panel-form ui-helper-clearfix">
                                <div class="ui-form-label">C&oacute;digo:</div>
                                <div class="ui-form-field">
                                    <input id="form2:codigo" type="text" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" disabled="disabled" readonly="readonly" style="width: 150px;">
                                </div>
                
                                <div class="ui-form-label">Descri&ccedil;&atilde;o:</div>
                                <div class="ui-form-field">
                                    <input id="form2:descricao" type="text" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" style="width: 300px;">
                                </div>
                
                                <div class="ui-form-label">Valor:</div>
                                <div class="ui-form-field">
                                    <input id="form2:valor" type="text" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" style="width: 150px;">
                                </div>
            
                                <div class="ui-form-footer">
                                    <button class="btnSalvar">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3><a href="#">Gr�ficos</a></h3>
                    <div class="app-chartTab">
                        <div id="chartBar"></div>
                        <div id="chartPie" style="width: 400px; height: 300px; margin: 30px auto 0px;"></div>
                    </div>
                </div>
            </div>
            <div class="ui-panel-footer ui-widget-content">Running jquery 1.8.0, jqueryui 1.8.23, SpringFramework 3.1.1 on Glassfish 3.1.2</div>
        </div>
                            
        <script type="text/javascript">
            var contextPath = "${pageContext.request.contextPath}";
            head.js(contextPath+"/resources/js/jquery-1.8.0.min.js",contextPath+"/resources/js/jquery-ui-1.8.23.min.js",
                contextPath+"/resources/js/json2.js", 
                contextPath+"/resources/js/jquery.jqplot.min.js", contextPath+"/resources/js/plugins/jqplot.barRenderer.min.js", 
                contextPath+"/resources/js/plugins/jqplot.highlighter.min.js", contextPath+"/resources/js/plugins/jqplot.categoryAxisRenderer.min.js", 
                contextPath+"/resources/js/plugins/jqplot.pieRenderer.min.js", 
                contextPath+"/resources/js/plugins/jqplot.pointLabels.min.js", contextPath+"/resources/js/app.js");
        </script>
    </body>
</html>
