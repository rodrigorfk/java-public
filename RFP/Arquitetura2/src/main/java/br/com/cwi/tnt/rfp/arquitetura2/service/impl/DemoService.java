/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.service.impl;

import br.com.cwi.tnt.rfp.arquitetura2.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura2.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura2.repository.GridEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura2.repository.SingleRowEntityDataRepository;
import br.com.cwi.tnt.rfp.arquitetura2.service.IDemoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author kuntzer
 */
@Service
public class DemoService implements IDemoService {

    @Autowired
    private GridEntityDataRepository gridEntityDataRepository;
    
    @Autowired
    private SingleRowEntityDataRepository singleRowEntityDataRepository;
    
    @Override
    public SingleRowEntity obterSingleRowEntity() {
        List<SingleRowEntity> lista = singleRowEntityDataRepository.obterTodos();
        if(!lista.isEmpty()){
            return lista.get(0);
        }
        return new SingleRowEntity();
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    public SingleRowEntity salvarSingleRowEntity(SingleRowEntity entity) {
        return singleRowEntityDataRepository.salvar(entity);
    }
    
    @Override
    public GridEntity obterGridEntityPorId(Long id) {
        return gridEntityDataRepository.obterPorId(id);
    }

    @Override
    public List<GridEntity> obterTodosGridEntity() {
        return gridEntityDataRepository.obterTodos();
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    public GridEntity salvarGridEntity(GridEntity entity) {
        return gridEntityDataRepository.salvar(entity);
    }
    
    @Override
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    public void excluirGridEntity(GridEntity entity) {
        gridEntityDataRepository.excluir(entity);
    }

    //<editor-fold defaultstate="collapsed" desc="setters">
    public void setGridEntityDataRepository(GridEntityDataRepository gridEntityDataRepository) {
        this.gridEntityDataRepository = gridEntityDataRepository;
    }
    
    public void setSingleRowEntityDataRepository(SingleRowEntityDataRepository singleRowEntityDataRepository) {
        this.singleRowEntityDataRepository = singleRowEntityDataRepository;
    }
    //</editor-fold>

    
}
