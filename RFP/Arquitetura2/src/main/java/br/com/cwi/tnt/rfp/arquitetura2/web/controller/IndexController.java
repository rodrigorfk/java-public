/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.web.controller;

import br.com.cwi.tnt.rfp.arquitetura2.entity.GridEntity;
import br.com.cwi.tnt.rfp.arquitetura2.entity.OpcaoCombo;
import br.com.cwi.tnt.rfp.arquitetura2.entity.SingleRowEntity;
import br.com.cwi.tnt.rfp.arquitetura2.service.IDemoService;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author kuntzer
 */
@Controller
public class IndexController {
    
    @Autowired
    private IDemoService demoService;
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String handleHome(ModelMap model){
        SingleRowEntity singleRow = demoService.obterSingleRowEntity();
        model.addAttribute("singleRow", singleRow);
        model.addAttribute("grid", demoService.obterTodosGridEntity());
        
        List<OpcaoCombo> opcaoCombos = new ArrayList<OpcaoCombo>();
        opcaoCombos.add(new OpcaoCombo("","Select one..."));
        opcaoCombos.add(new OpcaoCombo("ActionScript").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("AppleScript").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Asp").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("BASIC").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("C").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("C++").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Clojure").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("COBOL").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("ColdFusion").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Erlang").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Fortran").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Groovy").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Haskell").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Java").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("JavaScript").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Lisp").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Perl").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("PHP").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Python").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Ruby").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Scala").ajustSelected(singleRow.getCombo1()));
        opcaoCombos.add(new OpcaoCombo("Scheme").ajustSelected(singleRow.getCombo1()));
        model.addAttribute("opcaoCombos", opcaoCombos);
        
        return "index";
    }
    
    @RequestMapping(
            value="/index/singleRow", produces=MediaType.APPLICATION_JSON_VALUE, 
            consumes=MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody SingleRowEntity salvarSingleRowEntity(@RequestBody SingleRowEntity entity){
        demoService.salvarSingleRowEntity(entity);
        return entity;
    }
    
    @RequestMapping(
            value="/index/grid", produces=MediaType.APPLICATION_JSON_VALUE, 
            consumes=MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.POST
    )
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody GridEntity salvarGridEntity(@RequestBody GridEntity entity){
        demoService.salvarGridEntity(entity);
        return entity;
    }
    
    @RequestMapping(value="/index/grid/{id}", produces=MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void excluirGridEntity(@PathVariable Long id){
        GridEntity entity = demoService.obterGridEntityPorId(id);
        if(entity != null){
            demoService.excluirGridEntity(entity);
        }
    }
    
    @RequestMapping(value="/index/grid/suggest", produces=MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.GET)
    public @ResponseBody List<GridEntity> obterSuggestGridEntity(@RequestParam("term") String query){
        List<GridEntity> suggestions = new ArrayList<GridEntity>();  
          
        List<GridEntity> lista = demoService.obterTodosGridEntity();
        for(GridEntity p : lista) {  
            if(p.getDescricao().toLowerCase().startsWith(query.toLowerCase())){
                suggestions.add(p);  
            }
        }  
          
        return suggestions;  
    }
    
    @RequestMapping(value="/index/grid/all", produces=MediaType.APPLICATION_JSON_VALUE, method= RequestMethod.GET)
    public @ResponseBody List<GridEntity> obterTodosGridEntity(){
        return demoService.obterTodosGridEntity();
    }
}
