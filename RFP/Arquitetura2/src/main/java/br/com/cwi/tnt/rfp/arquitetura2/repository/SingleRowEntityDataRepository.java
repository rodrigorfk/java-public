/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.repository;

import br.com.cwi.framework.repository.JDBCBaseRepository;
import br.com.cwi.tnt.rfp.arquitetura2.entity.SingleRowEntity;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kuntzer
 */
@Repository
public class SingleRowEntityDataRepository extends JDBCBaseRepository {

    public List<SingleRowEntity> obterTodos() {
        return getJdbcTemplate().query("select sre.*, ge.descricao as descricaoAutocomplete from SingleRowEntity sre left join GridEntity ge on sre.autocomplete = ge.id order by sre.id", 
                new BeanPropertyRowMapper<SingleRowEntity>(SingleRowEntity.class));
    }

    public SingleRowEntity salvar(final SingleRowEntity entity) {
        if (entity.getId() == null) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                    PreparedStatement ps = cnctn.prepareStatement(
                            "insert into SingleRowEntity (texto1, combo1, autocomplete, dateDate, dateTime) values (?, ?, ?, ?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, entity.getTexto1());
                    ps.setString(2, entity.getCombo1());
                    if(entity.getAutocomplete() != null){
                        ps.setLong(3, entity.getAutocomplete());
                    }else{
                        ps.setNull(3, Types.INTEGER);
                    }
                    if(entity.getDateDate() != null){
                        ps.setDate(4, new Date(entity.getDateDate().getTime()));
                    }else{
                        ps.setNull(4, Types.DATE);
                    }
                    if(entity.getDateTime() != null){
                        ps.setTimestamp(5, new Timestamp(entity.getDateTime().getTime()));
                    }else{
                        ps.setNull(5, Types.TIMESTAMP);
                    }
                    return ps;
                }
            }, keyHolder);
            entity.setId(keyHolder.getKey().longValue());
        }else{
            getJdbcTemplate().update("update SingleRowEntity set texto1 = ?, combo1 = ?, autocomplete = ?, dateDate = ?, dateTime = ? where id = ?", 
                    entity.getTexto1(), entity.getCombo1(), entity.getAutocomplete(), entity.getDateDate(), entity.getDateTime(), entity.getId());
        }

        return entity;
    }
}
