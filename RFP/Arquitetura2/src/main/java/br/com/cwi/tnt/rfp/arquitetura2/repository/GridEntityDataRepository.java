/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.repository;

import br.com.cwi.framework.repository.JDBCBaseRepository;
import br.com.cwi.tnt.rfp.arquitetura2.entity.GridEntity;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kuntzer
 */
@Repository
public class GridEntityDataRepository extends JDBCBaseRepository {
    
    public List<GridEntity> obterTodos() {
        return getJdbcTemplate().query("select * from GridEntity order by id", new BeanPropertyRowMapper<GridEntity>(GridEntity.class));
    }
    
    public GridEntity obterPorId(Long id){
        return getJdbcTemplate().queryForObject("select * from GridEntity where id = ?", new BeanPropertyRowMapper<GridEntity>(GridEntity.class), id);
    }
    
    public void excluir(GridEntity entity){
        if(entity.getId() != null){
            getJdbcTemplate().update("delete from GridEntity where id = ?", entity.getId());
        }
    }
    
    public GridEntity salvar(final GridEntity entity) {
        if (entity.getId() == null) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                    PreparedStatement ps = cnctn.prepareStatement(
                            "insert into GridEntity (descricao, valor) values (?, ?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, entity.getDescricao());
                    if(entity.getValor() != null){
                        ps.setBigDecimal(2, entity.getValor());
                    }else{
                        ps.setNull(2, Types.NUMERIC);
                    }
                    return ps;
                }
            }, keyHolder);
            entity.setId(keyHolder.getKey().longValue());
        }else{
            getJdbcTemplate().update("update GridEntity set descricao = ?, valor = ? where id = ?", 
                    entity.getDescricao(), entity.getValor(), entity.getId());
        }

        return entity;
    }
}
