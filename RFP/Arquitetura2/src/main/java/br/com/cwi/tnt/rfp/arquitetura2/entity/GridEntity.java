/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.entity;

import br.com.cwi.framework.entity.BaseEntity;
import java.math.BigDecimal;

/**
 *
 * @author kuntzer
 */
public class GridEntity extends BaseEntity<Long> {
    
    private String descricao;
    private BigDecimal valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    
}
