/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.entity;

import br.com.cwi.framework.entity.BaseEntity;
import br.com.cwi.framework.utils.json.DateJsonSerializer;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author kuntzer
 */
public class SingleRowEntity extends BaseEntity<Long> {
    
    private String texto1;
    private String combo1;
    private Long autocomplete;
    private String descricaoAutocomplete;
    
    @JsonSerialize(using=DateJsonSerializer.class)
    private Date dateDate;
    private Date dateTime;

    public String getTexto1() {
        return texto1;
    }

    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    public String getCombo1() {
        return combo1;
    }

    public void setCombo1(String combo1) {
        this.combo1 = combo1;
    }

    public Long getAutocomplete() {
        return autocomplete;
    }

    public void setAutocomplete(Long autocomplete) {
        this.autocomplete = autocomplete;
    }

    public Date getDateDate() {
        return dateDate;
    }

    public void setDateDate(Date dateDate) {
        this.dateDate = dateDate;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescricaoAutocomplete() {
        return descricaoAutocomplete;
    }

    public void setDescricaoAutocomplete(String descricaoAutocomplete) {
        this.descricaoAutocomplete = descricaoAutocomplete;
    }
    
    
}
