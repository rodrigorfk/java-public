/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cwi.tnt.rfp.arquitetura2.entity;

import java.io.Serializable;

/**
 *
 * @author kuntzer
 */
public class OpcaoCombo implements Serializable {
    
    private String label;
    private String value;
    private Boolean selected = Boolean.FALSE;

    public OpcaoCombo(String value) {
        this(value,value);
    }

    public OpcaoCombo(String value, String label) {
        this.label = label;
        this.value = value;
    }

    public OpcaoCombo(String value, String label, Boolean selected) {
        this.label = label;
        this.value = value;
        this.selected = selected;
    }
    
    public OpcaoCombo(String value, Boolean selected) {
        this(value,value,selected);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
    
    public OpcaoCombo ajustSelected(String value){
        if(this.getValue() != null){
            this.setSelected(this.value.equals(value));
        }
        return this;
    }
    
}
