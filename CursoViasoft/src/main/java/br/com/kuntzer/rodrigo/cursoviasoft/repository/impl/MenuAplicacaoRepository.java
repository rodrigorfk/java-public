/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IMenuAplicacaoRepository;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rodrigokuntzer
 */
@Repository
public class MenuAplicacaoRepository extends JPABaseRepository<MenuAplicacao, Long> implements IMenuAplicacaoRepository {

	@Override
	public List<MenuAplicacao> obterMenusPrincipais() {
		return entityManager.createQuery("from MenuAplicacao where pai is null order by ordem", MenuAplicacao.class).getResultList();
	}
	
}
