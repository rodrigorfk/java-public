/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.BaseEntity;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IBaseRepository;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * 
 * @author rodrigokuntzer
 */
public abstract class JPABaseRepository<T extends BaseEntity<ID>, ID extends Serializable> implements IBaseRepository<T, ID> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> persistentClass;

	@SuppressWarnings("unchecked")
	public JPABaseRepository() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public List<T> obterTodos() {
		return entityManager.createQuery("from "+persistentClass.getSimpleName(), persistentClass).getResultList();
	}

	@Override
	public T salvar(T entity) {
		if(entity.getId() == null){
			entityManager.persist(entity);
		}
		return entityManager.merge(entity);
	}

	@Override
	public T obterPorId(ID id) {
		return entityManager.find(persistentClass, id);
	}

}
