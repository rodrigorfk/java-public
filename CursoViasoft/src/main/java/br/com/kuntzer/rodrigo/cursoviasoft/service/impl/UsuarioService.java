/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.service.impl;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import br.com.kuntzer.rodrigo.cursoviasoft.repository.IMenuAplicacaoRepository;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author rodrigokuntzer
 */
@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	private IMenuAplicacaoRepository menuAplicacaoRepository;
	
	@Override
	public List<MenuAplicacao> obterMenusPrincipais() {
		return menuAplicacaoRepository.obterMenusPrincipais();
	}
	
}
