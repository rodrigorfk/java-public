/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.repository;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.BaseEntity;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rodrigokuntzer
 */
public interface IBaseRepository<T extends BaseEntity<ID>, ID extends Serializable> {
	
	List<T> obterTodos();
	T salvar(T entity);
	T obterPorId(ID id);
}
