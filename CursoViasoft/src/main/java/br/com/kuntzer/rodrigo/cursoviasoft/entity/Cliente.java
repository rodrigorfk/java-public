/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author rodrigokuntzer
 */
@Entity
@Table(name="CLIENTE")
public class Cliente extends BaseEntity<ClienteId> {
	
	@Id
	@EmbeddedId
	private ClienteId id;
	
	@Column(name="NOME_CLIENTE", length=200, nullable=false, unique=true)
	private String nome;
	
	@Column(name="CPF_CNPJ", length=15, nullable=false)
	private String cpfCnpj;
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_PESSOA", length=10, nullable=false)
	private TipoCliente tipoCliente;
	
	@Column(length=150)
	private String endereco;
	
	@Column(length=150)
	private String cidade;
	
	@Column(length=2)
	private String uf;
	
	@OneToMany(mappedBy="cliente",cascade={CascadeType.ALL})
	private List<ClienteTelefone> telefones = new ArrayList<ClienteTelefone>();
	
	public ClienteId getId() {
		return id;
	}

	public void setId(ClienteId id) {
		this.id = id;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public List<ClienteTelefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<ClienteTelefone> telefones) {
		this.telefones = telefones;
	}
	
}
