/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.kuntzer.rodrigo.cursoviasoft.web.controller;

import br.com.kuntzer.rodrigo.cursoviasoft.entity.MenuAplicacao;
import br.com.kuntzer.rodrigo.cursoviasoft.service.IUsuarioService;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.separator.Separator;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;

/**
 *
 * @author rodrigokuntzer
 */
@Controller
@Scope("session")
public class HomeController {
	
	private static final Logger _logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private IUsuarioService usuarioService;
	
	private MenuModel menuModel;
	
	@PostConstruct
	public void init(){
		this.menuModel = new DefaultMenuModel();
		
		try{
			List<MenuAplicacao> menusPrincipais = usuarioService.obterMenusPrincipais();
			for(MenuAplicacao menuPrincipal:menusPrincipais){
				if(menuPrincipal.getFilhos().isEmpty()){
					MenuItem menuItem = new MenuItem();
					menuItem.setValue(menuPrincipal.getLabel());
					menuItem.setUrl(menuPrincipal.getUrl());
					menuItem.setIcon(menuPrincipal.getIcon());
					this.menuModel.addMenuItem(menuItem);
				}else{
					Submenu submenu = new Submenu();
					submenu.setLabel(menuPrincipal.getLabel());
					submenu.setIcon(menuPrincipal.getIcon());
					for(MenuAplicacao filho:menuPrincipal.getFilhos()){
						this.loadSubmenu(filho, submenu);
					}
					this.menuModel.addSubmenu(submenu);
				}
			}
		}catch(Exception ex){
			_logger.error("Erro ao carregar menus", ex);
		}
		_logger.info("Carregou menus!");
	}
	
	private void loadSubmenu(MenuAplicacao menu, Submenu submenu){
		if(menu.getFilhos().isEmpty()){
			if(!menu.getSeparador().booleanValue()){
				MenuItem menuItem = new MenuItem();
				menuItem.setValue(menu.getLabel());
				menuItem.setUrl(menu.getUrl());
				menuItem.setIcon(menu.getIcon());
				submenu.getChildren().add(menuItem);
			}else{
				Separator separator = new Separator();
				submenu.getChildren().add(separator);
			}
		}else{
			Submenu submenu2 = new Submenu();
			submenu2.setLabel(menu.getLabel());
			submenu2.setIcon(menu.getIcon());
			for(MenuAplicacao filho:menu.getFilhos()){
				this.loadSubmenu(filho, submenu2);
			}
			submenu.getChildren().add(submenu2);
		}
	}

	public MenuModel getMenuModel() {
		return menuModel;
	}
	
	
}
