package br.com.kuntzer.rodrigo.cursoviasoft.entity;

import java.io.Serializable;


public abstract class BaseEntity<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = -7865167993085665853L;

	public abstract ID getId();
	
	public abstract void setId(ID id);

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BaseEntity<?>)) {
			return false;
		}
		BaseEntity<?> other = (BaseEntity<?>) obj;
		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			} else {
				return super.equals(obj);
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

	
	@Override
	public String toString() {
		return this.getClass().getName() + "[id=" + getId() + "]";
	}
	
}
